"""
Create a list of all the consonants in the string "Yellow Yaks like yelling and yawning and yesturday they yodled while eating yuky yams"

"""
str1 = "Yellow Yaks like yelling and yawning and yesturday they yodled while eating yuky yams"
x=[set(element for element in str1.replace(" ", "") if element  not in "aeiouAEIOU")]
print(x)


