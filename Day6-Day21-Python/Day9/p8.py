# Python program to overload
# a comparison operators 

class A:
    def __init__(self, a):
        self.a=a
    def __gt__(self, obj):
        if self.a>obj.a:
            return True
        else:
            return False

obj1=A(10)
obj2=A(20)
if obj1>obj2:
    print("obj1 is grater than obj2.")
else:
    print("ob2 is greater than ob1")
