"""
Write a python program to generate a function to double a specified number.

The output is:

Specified number: 27.2

Expected Answer:
The double number of 27.2 = 54.4

"""

double_number=lambda number:"The double number of {} = {}".format(number, 2*number)
print(double_number(27.2))
