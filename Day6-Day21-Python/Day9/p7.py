# Python program to overload equality
# and less than operators

class A:
    def __init__(self, a):
        self.a=a
    def __lt__(self, obj):
        if self.a<obj.a:
            return "obj1 is less than obj2."
        else:
            return "ob2 is less than ob1"

obj1=A(10)
obj2=A(20)
print(obj1<obj2)
    
