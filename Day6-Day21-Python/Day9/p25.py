"""
Write a python program to generate a function to get the power of a specified number.

The output is:

Power of: 5, 6, 7
Specified number: 3

Expected Answer:
The power of five for number 3 = 15
The power of six for number 3 = 18
The power of seven for number 3 = 21

"""

#doubt
power=lambda n:n*3
print("The power of five for number 3 =",power(5))
print("The power of six for number 3 =",power(6))
print("The power of seven for number 3 =",power(7))
