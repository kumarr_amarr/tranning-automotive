"""
Write a python program to generate a lambda function to check whether a given string is a number or not. (True or False)

The output is:

Example of Expected Answer:
Is the given 3687 a number: True
Is the given Python a number: False

"""
is_digit=lambda string: string.isdigit()
print(is_digit("3687"))
print(is_digit("Python"))
