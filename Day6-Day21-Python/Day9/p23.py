"""
Write a  Python program to find if a given string starts with a given character using Lambda.
Sample Output:
True
False
"""

sw=lambda string, char:string.startswith(char)
print(sw("Lambda","a"))
