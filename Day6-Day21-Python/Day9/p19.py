"""
Lambda Function to find biggest of given two values.
"""
biggest=lambda a,b:f"The Biggest of {a},{b} is: {a}" if a>b else f"The Biggest of {a},{b} is: {b}"
print(biggest(5,2))
