"""
Get the index and the value as a tuple for items in the list 
["hi", 4, 8.99, 'apple', ('t,b','n')]. 
Result would look like [(index, value), (index, value)]

"""
list_a = ["hi", 4, 8.99, 'apple', ('t,b','n')]

x=[(list_a[i], i)for i in range(len(list_a))]
print(x)


