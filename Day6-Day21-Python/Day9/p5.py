class OperatorOverloading:
    def __init__(self, a):
        self.a=a

    def __add__(self, obj):
        return self.a+obj.a

obj1=OperatorOverloading(10)
obj2=OperatorOverloading(20)
#print(obj1+obj2)
#print(OperatorOverloading.__add__(obj1,obj2))
print(obj1.__add__(obj2))

obj3 = OperatorOverloading("Geeks")
obj4 = OperatorOverloading("For")
print(obj3+obj4)
