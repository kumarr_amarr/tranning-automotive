from abc import ABC, abstractmethod

class Account(ABC):
    def __init__(self, acc_no, acc_holder_name):
        self.acc_no=acc_no
        self.acc_holder_name=acc_holder_name
        self.balance =0
        
    @abstractmethod
    def deposit(self, amount):
        pass

    @abstractmethod
    def withdraw(self, amount):
        pass
        
    def balance_enquiry(self):
        return self.balance
        
class CurrentAccount(Account):
    def __init__(self, acc_no, acc_holder_name):
        super().__init__(acc_no, acc_holder_name)

    def deposit(self, amount):
        self.balance+=amount
        print(amount,'Rs. Credited in your Current Account')

    def withdraw(self, amount):
        self.balance-=amount
        print(amount,'Rs. debited from your Current Account')

    def process_to_cheque(self, cheque_no):
        print('Transaction Process through Cheque')

class SavingAccount(Account):
    def __init__(self, acc_no, acc_holder_name):
        super().__init__(acc_no, acc_holder_name)
        self.interest=0

    def deposit(self, amount):
        self.balance+=amount
        print(amount,'Rs. Credited in your Saving Account')

    def withdraw(self, amount):
        self.balance-=amount
        print(amount,'Rs. debited from your Saving Account')
        
    def monthly_interest(self):
        self.interst=self.balance*0.05


ca=CurrentAccount(12345,'John')
ca.deposit(10000)
print('Current Balance= ',ca.balance_enquiry())
ca.withdraw(500)
print('Current Balance= ',ca.balance_enquiry())

#Creating SavingAccount Object
sa=SavingAccount(784578,'Harry')
sa.deposit(5000)
print('Current Balance= ',sa.balance_enquiry())
sa.withdraw(500)
print('Current Balance= ',sa.balance_enquiry())



