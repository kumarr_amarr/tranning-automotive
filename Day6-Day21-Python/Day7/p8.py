class Circle:
    def draw(self):
        print('Circle Shape Draw!.')
class Rectangle:
    def paint(self):
        print('Rectangle Shape Paint!.')
class Square:
    def draw(self):
        print('Square Shape Draw!.')
class Triangle:
    def paint(self):
        print('Triangle Shape Paint!.')

def figure(obj):
    if hasattr(obj, "draw"):
        obj.draw()
    elif hasattr(obj, "paint"):
        obj.paint()
lst=[Circle(), Rectangle(), Square(), Triangle()]
for obj in lst:
    figure(obj)
    print('===========================')
