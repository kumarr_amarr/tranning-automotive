class Point:
    def __init__(self, x=0, y=0):
        self.x=x
        self.y=y
    def __str__(self):
        return "({0}, {1})".format(self.x, self.y)
    def __add__(self, other):
        x=self.x+other.x
        y=self.y+other.y
        return Point(x,y)
        
        
p1= Point(10,20)
p2=Point(30, 40)
p3=p1+p2
print('Point1 =',p1)
print('Point2= ',p2)
print('Sum of Points= ',p3)

