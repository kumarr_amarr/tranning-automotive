from abc import ABC, abstractmethod

class Polygon(ABC):
    @abstractmethod
    def no_of_sides(self):
        pass

class Triangle(Polygon):
    def no_of_sides(self):
         print("Triangle have 3 sides") 
    
class Pentagon(Polygon):
    def no_of_sides(self):
         print("Pentagon have 5 sides") 
    
class Hexagon(Polygon):
    def no_of_sides(self):
        print("Hexagon have 6 sides") 

class Quadrilateral(Polygon):
    def no_of_sides(self):
         print("Quadrilateral have 4 sides")

T = Triangle() 
T.no_of_sides() 
  
Q = Quadrilateral() 
Q.no_of_sides() 
  
P = Pentagon() 
P.no_of_sides() 
  
H = Hexagon() 
H.no_of_sides() 
