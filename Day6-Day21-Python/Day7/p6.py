class Circle:
    def draw(self):
        print("Circle Shape Draw!.")
    def color(self):
        print("Yellow Color Fill In Circle!. ")
class Rectangle:
    def draw(self):
        print("Rectangle Shape Draw!.")
    def color(self):
        print('Green Color Fill In Rectangle!.')
class Square:
    def draw(self):
        print('Square Shape Draw!.')
    def color(self):
        print('Pink Color Fill In Square!.')

def figure(obj):
    obj.draw()
    obj.color()
lst=[Circle(), Rectangle(), Square()]
for obj in lst:
    figure(obj)
    print('===========================')

