from abc import ABC, abstractmethod

class Shape(ABC):
    @abstractmethod
    def area(self):
        pass
    @abstractmethod
    def perimeter(self):
        pass
    
class Rectangle(Shape):
    def __init__(self, l, b):
        self.l=l
        self.b=b
    def area(self):
        return self.l*self.b
    def perimeter(self):
        return 2*(self.l+self.b)

class Square(Shape):
    def __init__(self, side):
        self.side=side
    def area(self):
        return self.side**2
    def perimeter(self):
        return 4*self.side

class Circle(Shape):
    def __init__(self, radius):
        self.radius=radius

    def area(self):
        return 3.14*self.radius**2
    def perimeter(self):
        return 2*3.14*self.radius

    
#Creating Rectangle Class Object
r1=Rectangle(10,20)
print('Area of Rectangle=',r1.area())
print('Perimeter of Rectangle=',r1.perimeter())
#Creating Square Class Object
s1=Square(10)
print('Area of Square=',s1.area())
print('Perimeter of Square=',s1.perimeter())

#Creating Circle Class Object
c1=Circle(1.2)
print('Area of Circle=',c1.area())
print('Perimeter of Circle=',c1.perimeter())
