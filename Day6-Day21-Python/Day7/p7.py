class Circle:
    def draw(self):
        print('Circle Shape Draw!.')
class Rectangle:
    def paint(self):
        print('Rectangle Shape Paint!.')
class Square:
    def draw(self):
        print('Square Shape Draw!.')
class Triangle:
    def paint(self):
        print('Triangle Shape Paint!.')

def figure(obj):
    obj.draw()
lst=[Circle(), Rectangle(), Square(), Triangle()]
for obj in lst:
    figure(obj)
    print('===========================')
