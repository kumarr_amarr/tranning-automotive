class Animal:
    def eating(self):
        print("Animal Eating...")
class Lion(Animal):
    def eating(self):
        print("Lion is eating meat.")
class Cow(Animal):
    def eating(self):
        print("Cow is eating grass.")

a=Animal()
a.eating()
l=Lion()
l.eating()
c=Cow()
c.eating()
