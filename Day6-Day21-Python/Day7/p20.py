from abc import ABC, abstractmethod

class Test1(ABC):
    @abstractmethod
    def m1(self):
        pass
    @abstractmethod
    def m2(self):
        pass
    @abstractmethod
    def m3(self):
        pass
class Test2(Test1):
    def m1(self):
        print('m1-method defined by Test2 Class')
    def m2(self):
        print('m2-method defined by Test2 Class')
    def m3(self):
        print('m3-method defined by Test2 Class')
t2 = Test2()
t2.m1()
t2.m2()
t2.m3()
