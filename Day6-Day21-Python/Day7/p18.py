from abc import ABC, abstractmethod
class C1(ABC):
    @abstractmethod
    def m1(self):
        pass

class C2(C1):
    def m1(self):
        print("Implemented in C2 class")

c1=C2()
c1.m1()
