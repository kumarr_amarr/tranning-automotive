class Parent:
    def property(self):
        print("Parent has money, Gold")
    def purchaseBike(self):
        print("Parent want to purchase Hero bike.")
    def marry(self):
        print("Parent decide to marry with black girl.")
class Child(Parent):
    def purchaseBike(self):
        print("Child want to purchase R-15 bike.")
    def marry(self):
        print("Child decide to marry with white girl.")

parent=Parent()
parent.property()
parent.purchaseBike()
parent.marry()
print("_______________________________________________")
child=Child()
child.property()
child.purchaseBike()
child.marry()
