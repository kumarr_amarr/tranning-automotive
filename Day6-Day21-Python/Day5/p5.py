class Phone:
    def __init__(self , brand, price, camera):
        self.brand=brand
        self.price=price
        self.camera=camera

    def buy(self):
        print("Buing a phone.")

    def return_(self):
        print("Returning a phone.")

class FeaturePhone(Phone):
    pass

class SmartPhone(Phone):
    def __init__(self, os, ram):
        self.os=os
        self.ram=ram
        print ("Inside SmartPhone constructor")

    def buy(self):
        print("Buing a smartphone.")

sp=SmartPhone("Android", 2)
print(sp.os)
print(sp.ram)
sp.buy()
sp.return_()
