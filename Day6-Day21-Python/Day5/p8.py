"""
Write a python function to add 'ing' at the end of a given string and return the new string. 
If the given string already ends with 'ing' then add 'ly'.
If the length of the given string is less than 3, leave it unchanged.

Sample Input   Expected Output
sleep          sleeping
amazing        amazingly
is             is
"""

def add__ing__ly_at_end(word):
    modified_word =""
    if len(word)<3:
        modified_word=word
    else:
        if word.endswith("ing"):
           modified_word=word+"".join("ly")
        else:
            modified_word=word+"".join("ing")
    return modified_word

word="sleep"
print(add__ing__ly_at_end(word))
word="amazing"
print(add__ing__ly_at_end(word))
word="is"
print(add__ing__ly_at_end(word))
