"""
Given two positive integers. Write a python function to return the greatest common divisor of the given numbers.
Sample Input    Expected Output
14 and 35       7
800 and 2800    400

"""

def hcf_of_two_number(n1, n2):
    factors_of_n1=[]
    factors_of_n2=[]
    
    if n1>0 and n2>0:
        for i in range(1,n1//2+1):
            if n1%i==0:
                factors_of_n1.append(i)

        for i in range(1,n2//2+1):
            if n2%i==0:
                factors_of_n2.append(i)

        if len(factors_of_n1)<len(factors_of_n2):
            return factors_of_n1[-1]
        else:
            return factors_of_n2[-1]
    else:
        return "Enter positive numbers only."

 
print(hcf_of_two_number(14, 35))
print(hcf_of_two_number(800, 2800))
