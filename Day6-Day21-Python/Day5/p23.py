"""
Given 2 positive integers, write a python function to return True if one of them is 10 or if their sum is 10, else return False.
Sample Input    Expected Output
10,9            True
2,8             True
2,9             False

"""

def is_sum_of_numbers_is_ten_or_one_of_them_is_ten(n1, n2):
    if n1 ==10 or n2==10:
        return True
    elif n1+n2==10:
        return True
    else:
        return False 
 
print(is_sum_of_numbers_is_ten_or_one_of_them_is_ten(10,9))
print(is_sum_of_numbers_is_ten_or_one_of_them_is_ten(2,8))
print(is_sum_of_numbers_is_ten_or_one_of_them_is_ten(2,9))
