"""
Write a python function to create and return a new dictionary from the given dictionary(item:price).
Given the following input, create a new dictionary with elements having price more than 200.
prices = {'ACME': 45.23,'AAPL': 612.78, 'IBM': 205.55,'HPQ': 37.20,'FB': 10.75}
Sample Input                                                                                    Expected Output
{ 'ACME': 45.23,'AAPL': 612.78, 'IBM': 205.55,'HPQ': 37.20,'FB': 10.75}                         {'AAPL': 612.78, 'IBM': 205.55}
{ 'MANGO': 150.45,'POMOGRANATE': 250.67, 'BANANA': 20.55,'CHERRY': 500.10,'ORANGE': 200.75}     {'ORANGE': 200.75, 'CHERRY': 500.1, 'POMOGRANATE': 250.67}




"""

def return_dict_for_more_then_a_given_price(d):
    res_dict={}
    for k, v in d.items():
        if v>=200:
            res_dict[k]=v
    return res_dict
    
    
dict1={ 'ACME': 45.23,'AAPL': 612.78, 'IBM': 205.55,'HPQ': 37.20,'FB': 10.75}
print(return_dict_for_more_then_a_given_price(dict1))
dict2={ 'MANGO': 150.45,'POMOGRANATE': 250.67, 'BANANA': 20.55,'CHERRY': 500.10,'ORANGE': 200.75}
print(return_dict_for_more_then_a_given_price(dict2))

