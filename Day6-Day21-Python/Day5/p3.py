class Phone:
    def __init__(self , brand, price, camera):
        self.brand=brand
        self.price=price
        self.camera=camera

    def buy(self):
        print("Buing a phone.")

    def return_(self):
        print("Returning a phone.")

class FeaturePhone(Phone):
    pass

class SmartPhone(Phone):
    pass

p=Phone("Apple",45000,"12MP")
print(p.brand)
p.buy()
p.return_()
