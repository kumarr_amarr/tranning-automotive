"""
Write a python function which accepts a sentence and finds the number of letters and digits in the sentence.
It should return a list in which the first value should be letter count and second value should be digit count.
Ignore the spaces or any other special character in the sentence.
Sample Input      Expected Output
Infosys 123       [7,3]
ABCEFG            [6,0]



"""

def number_of_chars_digits(sentence):
    sentence=sentence.split()
    sentence_without_space=""
    for word in sentence:
        sentence_without_space=sentence_without_space+"".join(word)
    num_digits=0
    num_of_chars=0
    for element in sentence_without_space:
        if element.isdigit():
            num_digits+=1
        else:
            num_of_chars+=1
    return [num_of_chars, num_digits]
    
    
sentence1="Infosys 123"
print(number_of_chars_digits(sentence1))
sentence2="ABCEFG"
print(number_of_chars_digits(sentence2))
