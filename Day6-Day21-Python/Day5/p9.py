"""
Write a python function which accepts a string containing a pattern of brackets and returns true if the pattern of brackets is correct. Otherwise it returns false.
The string of brackets is correct if it satisfies the following conditions:
1. Number of opening and closing brackets are equal.
2. Pattern should not start with closing bracket and end with opening bracket.
Sample Input    Expected Output
)()((()())      False
()((())())      True


"""

def correct_pattern_of_brackets(brackets):
    if brackets.startswith(")") and brackets.endswith("("):
        return False
    elif brackets.startswith("(") and brackets.endswith(")"):
        if brackets.count("(")==brackets.count(")"):
            return True
        else:
            return False
    else:
        return False

    
brackets=")()((()())"
print(correct_pattern_of_brackets(brackets))
brackets="()((())())"
print(correct_pattern_of_brackets(brackets))

brackets="()())(())"
print(correct_pattern_of_brackets(brackets))
