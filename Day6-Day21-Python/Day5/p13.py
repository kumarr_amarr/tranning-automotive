"""
Write a python function which accepts a list of numbers and returns true, if 1, 2, 3 appears in sequence in the list.
Otherwise, it should return false.
Sample Input
Expected            Output
[1, 1, 2, 3, 1]     True
[1, 1, 2, 4, 3]     False


"""

def sequence_of_one_two_three_in_list(l):
    for i in range(len(l)):
        if l[i]==1 and l[i+1] !=1:
            if l[i]==1 and l[i+1]==2 and l[i+2]==3:
                return True
            else:
                return False
    
l1=[1, 1, 2, 3, 1]
print(sequence_of_one_two_three_in_list(l1))
l2=[1, 1, 2, 4, 3]  
print(sequence_of_one_two_three_in_list(l2))
