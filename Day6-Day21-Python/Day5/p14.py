"""
Given two numbers, write a python function which returns true if first number is a seed of second number. Otherwise it returns false.
A number X is said to be a seed of number Y, if multiplying X by its each digit equates to Y
For example, 123 is a seed of 738 as 123*1*2*3 = 738.
Sample Input    Expected Output
123,738         True
45,1000         False
"""

def seed_of_first_number(n1, n2):
    num=n1
    digits_products=1
    while num>0:
        last_digit=num%10
        num//=10
        digits_products*=last_digit
        
    if digits_products*n1==n2:
        return True
    else:
        return False
    
print(seed_of_first_number(123, 738))
print(seed_of_first_number(45, 1000))
