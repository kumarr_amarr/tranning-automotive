class Phone:
    def __init__(self , brand, price, camera):
        self.brand=brand
        self.price=price
        self.camera=camera

    def buy(self):
        print("Buing a phone.")

    def return_(self):
        print("Returning a phone.")

class FeaturePhone(Phone):
    pass

class SmartPhone(Phone):
    def __init__(self, os, ram):
        self.os=os
        self.ram=ram
        super().__init__("Apple", 45000, "12MP")
        
sp=SmartPhone("Android",4)
print(sp.os)
print(sp.brand)
