"""
Given a list of numbers, write a python function which returns true if one of the first 4 elements in the list is 9. Otherwise it should return false.
The length of the list can be less than 4 also.
Sample Input        Expected Output
[1, 2, 9, 3, 4]     True
[1, 2, 9]           True
[1, 2,3,4]          False



"""

def check_a_specfic_element_in_specfic_possition_in_list(l):

    if l[0] ==9 or l[1]==9 or l[2]==9:
        return True
    else:
        return False
    
    
list1=[1, 2, 9, 3, 4] 
print(check_a_specfic_element_in_specfic_possition_in_list(list1))
list2=[1, 2, 9] 
print(check_a_specfic_element_in_specfic_possition_in_list(list2))
list3=[1, 2,3,4]
print(check_a_specfic_element_in_specfic_possition_in_list(list3))

