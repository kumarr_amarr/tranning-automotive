"""
Write a python function which accepts a list of strings containing details of deposits and withdrawals made in
a bank account and returns the net amount in the account.
Suppose the following input is supplied to the function 
["D:300","D:300","W:200","D:100"] where D means deposit and W means withdrawal,
then the net amount in the account is 500.
Sample Input                            Expected Output
["D:300","D:200","W:200","D:100"]       400
["D:350","W:100","W:500","D:1000"]      750


"""

def net_blabce(lst):
    all_element=[]
    total_withdrawls=0
    total_deposits=0
    for elements in lst:
        element=elements.split(":")
        for i in range(len(element)):
            all_element.append(element[i])
    for i in range(len(all_element)):
        if all_element[i]=="W":
            total_withdrawls+=int(all_element[i+1])
        elif all_element[i]=="D": 
            total_deposits+=int(int(all_element[i+1]))
    return total_deposits-total_withdrawls

lst1=["D:300","D:200","W:200","D:100"]
print(net_blabce(lst1))
lst2=["D:350","W:100","W:500","D:1000"]
print(net_blabce(lst2))
