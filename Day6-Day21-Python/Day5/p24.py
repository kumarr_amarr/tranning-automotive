"""
Given a list of numbers, write a python function to exchange the first n/2 elements of a list with the last n/2 elements. The function should return the new list.
n represents the number of elements in the list. Assume that it will always be even.
Sample Input                Expected Output
[1,2,3,4,5,6,7,8,9,10]      [10,9,8,7,6,1,2,3,4,5]



"""

def revers_last_half_in_first_half_remains_continue(lst):
    return lst[:len(lst)//2-1:-1]+lst[:len(lst)//2:]
 
print(revers_last_half_in_first_half_remains_continue([1,2,3,4,5,6,7,8,9,10]))
