class Phone:
    def __init__(self , brand, price, camera):
        self.brand=brand
        self.price=price
        self.camera=camera

    def buy(self):
        print("Buing a phone.")

    def return_(self):
        print("Returning a phone.")

class FeaturePhone(Phone):
    pass

class SmartPhone(Phone):
    pass

fp=FeaturePhone("Apple",45000,"12MP")
print(fp.brand)
fp.buy()
fp.return_()

sp=SmartPhone("Samsung", 20000, "16MP")
print(sp.price)
sp.buy()
sp.return_()
