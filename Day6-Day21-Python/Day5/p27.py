"""
Write a Python function to rotate the list of elements by N positions. The function should return the rotated list.
Sample Input                                Expected Output
Input list: [1, 2, 3, 4, 5, 6]
Number of positions to be rotated = 2       [5, 6, 1, 2, 3, 4]
Input list: [1, 2, 3, 4, 5, 6]
Number of positions to be rotated = 4       [3,4,5, 6, 1, 2]

Note : Assume that number of positions to be rotated is always a value >= 0 and <= length of the input list. 


"""

def rotate_list(lst, no_of_rotation):
    if not lst or no_of_rotation == 0:
        return lst
    no_of_rotation = no_of_rotation % len(lst)
    return lst[-no_of_rotation:] + lst[:-no_of_rotation]
   
print(rotate_list([1, 2, 3, 4, 5, 6], 2))
print(rotate_list([1, 2, 3, 4, 5, 6],4))
