"""
Given a list of integers and a number. Write a    python function to find and return the sum of the elements of the list. 
Note: Don't add the given number and also the numbers present before and after the given number in the list.
Sample input                            Expected output
list=[1,2,3,4], number=2                4
list=[1,2,2,3,5,4,2,2,1,2],number=2     5
list=[1,7,3,4,1,7,10,5],number=7        9
list=[1,2,1,2],number=2                 0

"""

def sum_of_numbers_with_condition(lst, n):
    indices_to_exclude = set()
    result_sum=0
    for i in range(len(lst)):
        if lst[i] == n:
            indices_to_exclude.add(i)
            if i > 0:
                indices_to_exclude.add(i - 1)
            if i < len(lst) - 1:
                indices_to_exclude.add(i + 1)

    for i in range(len(lst)):
        if i not in indices_to_exclude:
            result_sum+=lst[i]
    
    return result_sum

print(sum_of_numbers_with_condition([1, 2, 3, 4], 2))
print(sum_of_numbers_with_condition([1,2,2,3,5,4,2,2,1,2], 2))
print(sum_of_numbers_with_condition([1,7,3,4,1,7,10,5], 7))
print(sum_of_numbers_with_condition([1,2,1,2], 2))
