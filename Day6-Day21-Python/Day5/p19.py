"""
Write a python function which accepts a list of numbers and returns true if the list contains a 2 next to a 2. Otherwise it should return false.
Sample Input            Expected Output
[ 1,2,1,2,3,4,5,2,2]    True
[3,2,5,1,2,1,2]         False

"""

def two_next_to_two(lst):
    res=False
    for i in range(len(lst)-1):
        if lst[i]==2:
            if lst[i]==2 and lst[i+1]!=2:
                continue
            else:
                res= True
    return res

print(two_next_to_two([ 1,2,1,2,3,4,5,2,2]))
print(two_next_to_two([3,2,5,1,2,1,2]))
print(two_next_to_two([3,3,5,1,2,1,2]))
