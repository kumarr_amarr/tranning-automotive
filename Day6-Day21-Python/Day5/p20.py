"""
Write a python function to find out whether a number is divisible by the sum of its digits. If so return True,else return False.
Sample Input    Expected Output
42              True
66              False

"""

def is_number_divisible_by_sum_of_its_digit(number):
    original_number=number
    sum_of_digits=0
    while number>0:
        last_digit=number%10
        sum_of_digits+=last_digit
        number//=10
    if original_number % sum_of_digits==0:
        return True
    else:
        return False
        


    
print(is_number_divisible_by_sum_of_its_digit(42))
print(is_number_divisible_by_sum_of_its_digit(66))
