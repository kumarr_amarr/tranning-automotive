"""
Write a python function that accepts a list of words and returns the list of integers representing the length of the corresponding words. 

Sample Input    Expected Output
[cat, Come]     [3,4]

"""

def length_of_words_in_list(lst):
    words_length=[]
    for word in lst:
        count=0
        for char in word:
            count+=1
        words_length.append(count)
    return words_length
 
print(length_of_words_in_list(["cat", "Come"]))
