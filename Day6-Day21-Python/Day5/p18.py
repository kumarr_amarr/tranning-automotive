"""
Write a python function which accepts a sentence and returns a list in which first value is the count of upper case letters and second value is
the count of lower case letters in the sentence. Ignore spaces, numbers and other special characters if any.
Sample Input        Expected Output
Hello world!        [1,9]
Welcome to Mysoren  [2,14]

"""

def count_of_voele_and_consonent(sentence):

    a_z="abcdefghijklmnopqrstuvwxyz"
    A_Z=a_z.upper()
    upper_letters=0
    lower_letters=0
    special_letters=0
    for char in sentence:
        if char in A_Z:
            upper_letters+=1
        elif char in a_z:
            lower_letters+=1
        else:
            special_letters+=1
    return [upper_letters, lower_letters]


print(count_of_voele_and_consonent("Hello world!"))
print(count_of_voele_and_consonent("Welcome to Mysoren"))
