"""
Write a Python program where a string will start with a specific number.
"""

import re
string="11Python"
pattern=re.compile(r"^15\w+$")
match=bool(re.match(pattern, string))
print(match)
