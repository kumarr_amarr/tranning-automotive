"""
Write a Python program to abbreviate 'Road' as 'Rd.' in a given string.
"""
street = '21 Ramkrishna Road'
import re
matches=re.sub(r"\bRoad\b", "Rd.", street)
print(matches)
