"""
Write a Python program that matches a word at the end of string, with optional punctuation.
"""

import re
string="What's up?"
pattern=re.compile(r"\w+[.|?|!]$")
match=bool(re.search(pattern, string))
print(match)
