"""
 Write a Python program to search some literals strings in a string.
 Go to the editor Sample text : 'The quick brown fox jumps over the lazy dog.' Searched words : 'fox', 'dog', 'horse'
"""

import re
string='The quick brown fox jumps over the lazy dog.'
patterns=["fox", "dog", "horse"]
for pattern in patterns:
    match=re.search(pattern, string)
    if match:
        print("Matched")
    else:
        print("Not matched")
