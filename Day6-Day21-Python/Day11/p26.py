"""
Write a Python program to convert a date of yyyy-mm-dd format to dd-mm-yyyy format.

"""
import re

y_m_d="2001-02-23"
pattern= re.compile(r"^(\d{4})-(\d{2})-(\d{2})$")
match=pattern.match(y_m_d)
print(match.groups())
print(match)
if match:
    year, month, day=match.groups()
    d_m_y=f"{day}-{month}-{year}"
    print(d_m_y)
else:
    print("Invalid date format")
