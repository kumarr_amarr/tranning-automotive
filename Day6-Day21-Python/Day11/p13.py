"""
Write a Python program that matches a word containing 'z'
"""

import re
string="zebra in zoo "
pattern=re.compile(r"\w*z+\w*")
match=bool(re.search(pattern, string))
print(match)
