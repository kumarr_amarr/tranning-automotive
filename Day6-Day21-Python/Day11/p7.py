"""
Write a Python program that matches a string that has an a followed by two to three 'b'.
"""

import re
string="Amarabjwdhabbbb"
pattern=re.compile(r"[a][b]{2,3}")
matches_lst=re.findall(pattern, string)
print(matches_lst)
