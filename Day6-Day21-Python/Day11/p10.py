"""
Write a Python program that matches a string that has an 'a' followed by anything, ending in 'b'.
"""

import re
string="bananba"
pattern=re.compile(r"[a].[b$]")
matches_lst=re.findall(pattern, string)
print(matches_lst)
