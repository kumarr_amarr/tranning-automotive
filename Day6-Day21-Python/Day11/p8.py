"""
Write a Python program to find sequences of lowercase letters joined with a underscore.
"""

import re
string="Aamar_kumar"
pattern=re.compile(r"[a-z]+_[a-z]+")
matches_lst=re.findall(pattern, string)
print(matches_lst)
