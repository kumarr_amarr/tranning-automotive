"""
Write a Python program to find the occurrence and position of the substrings within a string.
"""

import re
string='The quick brown fox jumps over the lazy dog.'
pattern="e"
matches=re.finditer(pattern, string)
occurrence=0
for match in matches:
    occurrence+=1
    if match:
        print("Matched at:",match.start(),"to", match.end())
    else:
        print("Not matched")
print(f"Found at {occurrence} times.")
