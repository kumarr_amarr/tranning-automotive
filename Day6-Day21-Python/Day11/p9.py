"""
Write a Python program to find the sequences of one upper case letter followed by lower case letters.
"""

import re
string="Amar_kumar"
pattern=re.compile(r"[A-Z][a-z]+")
matches_lst=re.findall(pattern, string)
print(matches_lst)
