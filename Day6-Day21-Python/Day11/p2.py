"""
Write a Python program to check that a string contains only a certain set of characters (in this case a-z, A-Z and 0-9).
"""

import re
string="Python 1991"
pattern=re.compile(r"[a-zA-Z0-9]+")
matches=re.finditer(pattern, string)
if matches:
    print("Found")
else:
    print("Not found.")
for match in matches:
    print(match.group())
