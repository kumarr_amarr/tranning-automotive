"""
 Write a Python program that matches a word containing 'z', not at the start or end of the word.
"""

import re
string="abzra"
pattern=re.compile(r"^[a-yA-Y]+[a-zA-Z]*[z]+[a-yA-Y]+$")
match=bool(re.match(pattern, string))
print(match)
