"""
Write a Python program to match if two words from a list of words starting with letter 'P'.
"""
import re
lst=["Apple", "Pen", "Banana", "Papaya"]
pattern="^[P][A-Za-z]+"
new_lst=[]
for word in lst:
    matches=re.findall(pattern, word)
    new_lst+=matches
if len(new_lst)>=2:
    print("Two words starting with 'p' have found.")
else:
    print("Two words starting with 'p' have not found.")
        




