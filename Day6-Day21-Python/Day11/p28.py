"""
Write a Python program to separate and print the numbers of a given string.
"""
text="Python was created in 1991 and Java was creared in 1995."
import re
pattern=re.compile(r"\d+")
matches=pattern.findall(text)
for match in matches:
    print(match)
