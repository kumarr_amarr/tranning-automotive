"""
Write a Python program that matches a string that has an a followed by zero or more b's
"""

import re
string="Amarabjwdhabbbb"
pattern=re.compile(r"[a][b]*")
matches_lst=re.findall(pattern, string)
print(matches_lst)
