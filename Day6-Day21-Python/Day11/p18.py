"""
Write a Python program to check for a number at the end of a string.
"""

import re
string="Amar123"
pattern=re.compile(r"^[.]*[0-9]+$", re.IGNORECASE)
match=bool(re.search(pattern, string))
print(match)
