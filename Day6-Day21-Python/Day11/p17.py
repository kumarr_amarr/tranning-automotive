"""
Write a Python program to remove leading zeros from an IP address.
"""

import re
ip="192.00168.0028.00201"
pattern=re.compile(r"\.[0]*")
correct_ip=re.sub(pattern, "." ,ip)
print(correct_ip)
help(re.sub)
