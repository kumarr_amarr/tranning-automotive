"""
Write a Python program to find the substrings within a string.
Sample text :
'Python exercises, PHP exercises, C# exercises'

Pattern :

'exercises'

Note: There are two instances of exercises in the input string.
"""

import re
string='Python exercises, PHP exercises, C# exercises'
pattern="exercises"
matches=re.finditer(pattern, string)
for match in matches:
    if match:
        print("Matched at:",match.start(),"to", match.end(),"=", match.group())
    else:
        print("Not matched")
