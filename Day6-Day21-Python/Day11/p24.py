import re
string='The quick brown fox_jumps over the lazy dog.'
patterns=["\\s", "_", "##"]
for pattern in patterns:
    if pattern=="\\s":    
        string=re.sub(pattern, "##", string)
    elif pattern=="_":
        string=re.sub(pattern, " ", string)
    else:
        string=re.sub(pattern, "_", string)
print(string)
