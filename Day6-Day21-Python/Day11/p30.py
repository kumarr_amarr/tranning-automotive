"""
Write a Python program to separate and print the numbers and their position of a given string.
"""
text="Python was created in 1991 and Java was creared in 1995."
import re
pattern=re.compile(r"\d+")
matches=pattern.finditer(text)
for match in matches:
    print(match.start(), match.end(),"--->", match.group())
