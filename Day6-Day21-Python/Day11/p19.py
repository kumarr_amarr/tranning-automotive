"""
Write a Python program to search the numbers (0-9) of length between 1 to 3 in a given string.
"""

import re
string="Python is a powerful language. It was creatrd bu Guido Van Rossum in 1991"
pattern=re.compile(r"[0-9]{1,3}")
matches=re.findall(pattern, string)
for match in matches:
    print(match)
