"""
Write a Python program to match a string that contains only upper and lowercase letters, numbers, and underscores.
"""

import re
string="ze_B"
pattern=re.compile(r"^\w+$")
match=bool(re.match(pattern, string))
print(match)
