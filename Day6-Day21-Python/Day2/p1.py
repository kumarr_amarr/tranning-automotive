"""
Write a Python  program to print Fibonacci series up to n terms
Input
Input number of terms: 10
Output
Fibonacci series: 
0, 1, 1, 2, 3, 5, 8, 13, 21, 34

"""

def fabonacci(terms):
    if terms<=0:
        return
    elif terms==1:
        return 0
    v1=0
    v2=1
    series=[v1,v2]
    for i in range(terms-2):
        next_v=v1+v2
        series.append(next_v)
        v1=v2
        v2=next_v
    return series

print("Fibonacci series:",fabonacci(0))
    
 


        
















    
