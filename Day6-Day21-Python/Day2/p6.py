"""
Write a program to accept a sentence which may be terminated by either ‘.’ or ‘?’ only.
The words are to be separated by a single blank space.
Print an error message if the input does not terminate with ‘.’ or ‘?’.
You can assume that no word in the sentence exceeds 15 characters, so that you get a proper formatted output.
 
Perform the following tasks:
(i) Convert the first letter of each word to uppercase.
(ii) Find the number of vowels and consonants in each word and display them
with proper headings along with the words.
 
Test your program with the following inputs.
 
Example 1
 
INPUT: Intelligence plus character is education.
 
OUTPUT:
Intelligence Plus Character Is Education
 
Word	Vowels	Consonants
Intelligence	5	7
Plus	1	3
Character	3	6
Is	1	1
Education	5	4


"""
def count_vowel_consonant(sentence):

    if not (sentence.endswith('.') or sentence.endswith('?') or sentence.endswith('!')):
        print("Sentence must end with either '.' or '?' or '!'")
        return

    sentence = sentence[:-1].title()
    print(sentence)


    print("Word\t\tVowels consonants")
    for word in sentence.split():
        vowel_count=0
        consonant_count=0
        for vowel in word.lower():
            if vowel == "a" or vowel == "e" or vowel == "i" or vowel == "o" or vowel == "u":
                vowel_count+=1
            else:
                consonant_count+=1
        print(word,"\t",vowel_count,"\t",consonant_count)

sentence = "Intelligence plus character is education."

count_vowel_consonant(sentence)








