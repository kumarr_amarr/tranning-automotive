num=[10,20,30,40,50,60]
print(num)
print(num[1])
print(num[0])
print(num[-1])
print("_________________")
for i in range(len(num)):
    print(num[i])
print("_________________")

for i in num:
    print(i)
print("_________________")

names=["Amar", "Rahul", "Gaurav", "Santosh"]
for name in names:
    print(name)
print("_________________")

for i in range(len(names)):
    print(names[i])

nummbers=[10,20,30,40,50,60]
#n=eval(input("Enter list"))
#print(n)

list1 = []
print(list1)
list2=[1]*10
print(list2)

list3=list("Amar")
print(list3)

print(list("1236540451287"))

print(list(range(50,101,2)))

print(type(list3),id(list3))
list3.append(100)
print(type(list3),id(list3))

mixed_list=["Amar",205.65, 20, None, True, 0, False]
print(mixed_list)
for (i, v) in enumerate(mixed_list):
    print(i, v)

for i in range(0, len(mixed_list),1):
    print(i,"---->", mixed_list[i])


chars=list("Pythonisfunny")
print(chars)
print(chars[0:len(chars):1])
print(chars[1:5:2])
print(chars[:4:1])
print(chars[4::1])
print(chars[::])
print(chars[::-1])
print(chars[-8:-12:-1])
print(chars[-8:2:-1 ])
print(chars[5:-12:-1])
print(chars[-9:])
print(chars[-9::-1])

lst1=[10,20,30]
lst2=[40,50,60]
print(lst1+lst2)
for i, j in zip(lst1, lst2):
    print(i,j)

lst1=list("abc")
lst2=list("xyzk")
for i, j in zip(lst1, lst2):
    print(i, j)
lst1=["amar", "ram", "mohan"]*5
print(lst1)
lst2=[5,6,4]*3
print(lst2)




