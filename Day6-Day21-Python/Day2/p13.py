"""
Write a python program to display all the common characters between two strings. Return -1 if there are no matching characters.
Note: Ignore blank spaces if there are any. Perform case sensitive string comparison wherever necessary.
Sample Input	Expected output
"I like Python"
"Java is a very popular language"	lieyon

"""
def common_chars_betwe_two_string(str1, str2):
    new_str=""
    for i in str1:
        if i in str2:
            new_str=new_str+"".join(i).strip()
    return new_str
    
s1="I like Python"
s2="Java is a very popular language"

print(common_chars_betwe_two_string(s1,s2))
