"""
Problem Statement
Write a python program which displays the count of the names that matches a given pattern from a list of names provided.
Consider the pattern characters to be:
1. "_ at" where "_" can be one occurrence of any character
2. "%at%" where "%" can have zero or any number of occurrences of a character
Sample Input	Expected Output
[Hat, Cat, Rabbit, Matter]	_at -> 2
%at% -> 3

"""
def display_names_count(names):
    pattern=["_at", "%at%"]
    count1=0
    count2=0
    for name in names:
        if name[1:]=="at":
            count1=count1+1
    for name in names:
        if "at" in name:
            count2=count2+1
    print("_at ->",count1)
    print("%at% ->",count2)

names =["Hat", "Cat", "Rabbit", "Matter"]
display_names_count(names)
