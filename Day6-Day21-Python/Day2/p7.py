"""
Write a program to accept a sentence which may be terminated by either’.’, ‘?’or’!’ only.
The words may be separated by more than one blank space and are in UPPER CASE.
Perform the following tasks:
(a) Find the number of words beginning and ending with a vowel.
(b) Place the words which begin and end with a vowel at the beginning, followed by the remaining words as they occur in the sentence.
Test your program with the sample data and some random data:
Example 1
INPUT: ANAMIKA AND SUSAN ARE NEVER GOING TO QUARREL ANYMORE.
OUTPUT: NUMBER OF WORDS BEGINNING AND ENDING WITH A VOWEL= 3
ANAMIKA ARE ANYMORE AND SUSAN NEVER GOING TO QUARREL
Example 2
INPUT: YOU MUST AIM TO BE A BETTER PERSON TOMORROW THAN YOU ARE TODAY.
OUTPUT: NUMBER OF WORDS BEGINNING AND ENDING WITH A VOWEL= 2
A ARE YOU MUST AIM TO BE BETTER PERSON TOMORROW THAN YOU TODAY
Example 3
INPUT: LOOK BEFORE YOU LEAP.
OUTPUT: NUMBER OF WORDS BEGINNING AND ENDING WITH A VOWEL= 0
LOOK BEFORE YOU LEAP
Example 4
INPUT: HOW ARE YOU@
OUTPUT: INVALID INPUT

"""
def count_vowel_beginning_end(sentence):
    if not (sentence.endswith(".") or sentence.endswith("?") or sentence.endswith("!")):
        print("INVALID INPUT")
        return
    sentence=sentence[:-1].upper().split()
    vowels={"A", "E", "I", "O", "U"}
    vowel_words=[]
    other_words=[]

    for word in sentence:
        word = word.strip()
        if word[0] in vowels and word[-1] in vowels:
            vowel_words.append(word)
        else:
            other_words.append(word)
            
    reordered_sentence=vowel_words+other_words
    print(f"NUMBER OF WORDS BEGINNING AND ENDING WITH A VOWEL= {len(vowel_words)}")
    print(" ".join(reordered_sentence))

count_vowel_beginning_end("ANAMIKA AND SUSAN ARE NEVER GOING TO QUARREL ANYMORE.")
count_vowel_beginning_end("YOU MUST AIM TO BE A BETTER PERSON TOMORROW THAN YOU ARE TODAY.")
count_vowel_beginning_end("LOOK BEFORE YOU LEAP.")
count_vowel_beginning_end("HOW ARE YOU@")
