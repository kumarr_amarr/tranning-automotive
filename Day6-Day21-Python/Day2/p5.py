"""
Write a program to accept a sentence which may be terminated by either ‘.’ ‘?’ or ‘!’ only. Any other character may be ignored.
The words may be separated by more than one blank space and are in UPPER CASE.
Perform the following tasks:
(a)  Accept the sentence and reduce all the extra blank space between two words to
a single blank space.
(b) Accept a word from the user which is part of the sentence along with its
position number and delete the word and display the sentence.
Test your program with the sample data and some random data:
Example 1
 
INPUT:      
A    MORNING WALK IS A IS BLESSING FOR   THE  WHOLE DAY.
 
WORD TO BE DELETED: IS
WORD POSITION IN THE SENTENCE: 6
 
OUTPUT:  
 
A MORNING WALK IS A BLESSING FOR THE WHOLE DAY.
 
Example 2
 
INPUT:        
 
AS YOU    SOW, SO   SO YOU REAP.
 
WORD TO BE DELETED: SO
WORD POSITION IN THE SENTENCE: 4

"""
def normal_sentence(sentence):
    sentence = " ".join(sentence.split())
    return sentence


def delete_word(sentence, word_to_delete, position):
    words=sentence.split()
    if position<1 or position>len(words):
        return "Invalid position"
    
    if words[position-1]==word_to_delete:
        words.pop(position-1)
    else:
        return "Word at the given position does not match the word to be deleted"
    
    return ' '.join(words)



sentence= "A    MORNING WALK IS A IS BLESSING FOR   THE  WHOLE DAY."
normal_sentence= normal_sentence(sentence)
word_to_delete= "IS"
position= 6

output= delete_word(normal_sentence, word_to_delete, position)
print(output)
