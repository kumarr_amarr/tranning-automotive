"""
"""
name="Amarr"
print("A" in name)
print("s" not in name)
print(name.upper())
print(name.lower())
print(name.casefold())
print(name.endswith("r"))
print(name.endswith("b"))
print(name.startswith("a"))
print(name.startswith("A"))
print(name.count("r"))
print(name.find("r"))
print(name.index("r"))
print(name.find("s"))
#print(name.index("s")) 
print(name.swapcase())

string="Python is funny"
print(string.title())
print(string.istitle())
print(string.split())
print(string.split("n"))
print("   Amar".strip())
print("   Amar    ".strip())

print("AMAR".isupper())
print("Amar".isupper())
print("amar".islower())
print("Amar".islower())
print("2001".isdigit())
print("Amar123".isdigit())
print("Amar123"[-1:-3:-1].isdigit())
print("Amar123"[4:].isdigit())
print("Amar123".isalnum())
print("Amar".isalpha())
print("Amar12".isalpha())

























    
