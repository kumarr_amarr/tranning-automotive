"""
The encryption of alphabets are to be done as follows:
A = 1
B = 2
C = 3
.
.
.
Z = 26
 
The potential of a word is found by adding the encrypted value of the alphabets.
Example: KITE
Potential = 11 + 9 + 20 + 5 = 45
Accept a sentence which is terminated by either “ . ” , “ ? ” or “ ! ”. Each word of sentence is separated by single space. Decode the words according to their potential and arrange them in ascending order.
Output the result in format given below:
Example 1
 
INPUT       :   THE SKY IS THE LIMIT.
 
POTENTIAL   :   THE     = 33
               SKY     = 55
               IS      = 28
               THE     = 33
               LIMIT   = 63
 
OUTPUT      :   IS THE THE SKY LIMIT


"""
def calculate_potential(word):
    total = 0
    uppercase_word = word.upper()
    for char in uppercase_word:
        value = ord(char) - ord('A') + 1
        total += value
    return total

def process_sentence(sentence):
    words = sentence[:-1].split()
    potentials = [(word, calculate_potential(word)) for word in words]
    sorted_words = sorted(potentials, key=lambda x: x[1])
    potential_output = "\n".join(f"{word} = {potential}" for word, potential in potentials)
    sorted_words_output = " ".join(word for word, _ in sorted_words)
    return potential_output, sorted_words_output

sentence = "THE SKY IS THE LIMIT."
potential_output, sorted_words_output = process_sentence(sentence)
print(potential_output)
print(sorted_words_output)
