"""
Write a python function, encrypt_sentence() which accepts a message and encrypts it based on rules given below and returns the encrypted message.

Words at odd position -> Reverse It
Words at even position -> Rearrange the characters so that all consonants appear before the vowels and their order should not change

Note: 
1.	Assume that the sentence would begin with a word and there will be only a single space between the words.
2.	Perform case sensitive string operations wherever necessary.
Sample Input	Expected Output
the sun rises in the east	eht snu sesir ni eht stea

"""
#Doubt
def encrypt_sentence(message):
    vowels="a,e,i,o,u"
    consonant=""
    vowel=""
    message=message.split()
    encrypted_message=[]
    for i in range(0,len(message)):
        if i%2==0:
            encrypted_message.append(message[i][::-1])
        else:
            for char in message[i]:
                if char in vowels:
                    vowel =vowel.join(char)
                else:
                    consonant=consonant.join(char)
            encrypted_message.append(consonant+vowel)
            
    return encrypted_message
        
s1="the sun rises in the east"

print(encrypt_sentence(s1))
