"""
Given a list of integer values. Write a python program to check whether it contains same number in adjacent position. Display the count of such adjacent occurrences
Sample Input	Expected Output
[1,1,5,100,-20,-20,6,0,0]	3
[10,20,30,40,30,20]	0
[1,2,2,3,4,4,4,10]	3


"""
def count_adjacent_position_numbers_occurrences(numbers):
    count=0
    for i in range(1,len(numbers)):
        if numbers[i]==numbers[i-1]:
            count= count+1
    return count
    
num =[1,1,5,100,-20,-20,6,0,0]
print(count_adjacent_position_numbers_occurrences(num))
num =[10,20,30,40,30,20]
print(count_adjacent_position_numbers_occurrences(num))
num =[1,2,2,3,4,4,4,10]
print(count_adjacent_position_numbers_occurrences(num))
