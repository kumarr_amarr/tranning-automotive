class Math:
    @staticmethod
    def factorial(n):
        if n==0:
            return 1
        else:
            return n*Math.factorial(n-1)
factorial = Math.factorial(5)
print(factorial)
