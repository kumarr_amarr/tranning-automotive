class Demo:
    def __init__(self):
        self.a=10
        self.b=20

# instantiate the Demo class
d1=Demo()
print("Before changing d1 => ",d1.a,d1.b)
d1.a=30
d1.b=40
d2=Demo()
print("For d2=> ",d2.a,d2.b)
print("===================================")
print("After changing d1 => ",d1.a,d1.b)
print("For d2=> ",d2.a,d2.b)
