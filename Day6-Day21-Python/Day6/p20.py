class Student:
    def __init__(self, name, roll):
        self.name=name
        self.roll=roll

    @classmethod
    def m1(cls):
        Student.college_name="ITM"

    @classmethod
    def m2(cls):
        cls.college_code="26010"
     
s1=Student("Raj", 101)
Student.m1()
Student.m2()
print(s1.college_name, s1.college_code, s1.name, s1.roll, id(s1.college_name))

s2=Student("Amar", 102)
print(s2.college_name, s2.name, s2.roll, id(s2.college_name))
