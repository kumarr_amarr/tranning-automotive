class Car:
    Type1= "Four Wheeler"
    def __init__(self, name, old):
        self.name=name
        self.old=old

Maruti = Car("Maruti", 14)
Tata = Car("Tata", 13)

print("Maruti is a {}.".format(Maruti.__class__.Type1))
print("Tata is a {}.".format(Tata.__class__.Type1))
print("Tata is a {}.".format(Tata.Type1))

print("{} is {} years old.".format(Maruti.name, Maruti.old))
print("{} is {} years old.".format(Tata.name, Tata.old))
