"""
Program to track the number of objects created for a class:
"""

class Demo:
    count=0
    def __init__(self):
        Demo.count+=1
    @classmethod
    def noOfObjects(cls):
        print('The number of objects created for Demo class:',cls.count)

d1=Demo()
d2=Demo()
Demo.noOfObjects()
d3=Demo()
d4=Demo()
d5=Demo()
Demo.noOfObjects()
