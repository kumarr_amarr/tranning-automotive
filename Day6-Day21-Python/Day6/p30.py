class Student:
    college_name="ITM"
    college_code="26010"
    
s1=Student()
print("Before:")
print(Student.college_name)
print(Student.college_code)

Student.college_name="IIT"
Student.college_code="26000"
print("After:")
print(Student.college_name)
print(Student.college_code)
