class Demo:
    def __init__(self):
        self.a=10
        self.b=20

        self.c=30
        self.d=40
        self.e=50
# instantiate the Demo class
d1=Demo()
d2=Demo()
print(d1.__dict__)
print(d2.__dict__)
print("========================================")
del d1.a
print(d1.__dict__)
print(d2.__dict__)
