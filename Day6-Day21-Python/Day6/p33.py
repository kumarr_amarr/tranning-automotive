class Person:
    def setName(self, name):
        self.name=name

    def setAge(self, age):
        self.age=age

    def getName(self):
        return self.name

    def getAge(self):
        return self.age

p1=Person()
p1.setName("Amar")
p1.setAge(23)

print("Name:", p1.getName())
print("Age:", p1.getAge())
