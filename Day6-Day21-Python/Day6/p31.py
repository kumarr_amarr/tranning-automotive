"""
Let us say we want to assign a unique number to each product object. The
first product object should be given a number 100 and subsequent product
objects should have that value increased by 1. We can accomplish this by
using a combination of static and instance variables as shown below:

"""
class Product:
    product_id=100

    def __init__(self, name, price, brand):
        self.name=name
        self.price=price
        self.brand=brand
        Product.product_id+=1
        
    def displayPrdouctDetails(self):
        print("Product Id:",Product.product_id)
        print("Product Name:",self.name)
        print("Product Price:",self.price)
        print("Product Brand:",self.brand)

prod1=Product('Ipad',10000,'Apple')
prod1.displayPrdouctDetails()
print("============================")
prod2=Product('Redmi4A',10000,'Redmi')
prod2.displayPrdouctDetails()
print("============================")
prod3=Product('Vivo T2x',13000,'Vivo')
prod3.displayPrdouctDetails()
print("============================")
prod4=Product('F17',18000,'Oppo')
prod4.displayPrdouctDetails()
