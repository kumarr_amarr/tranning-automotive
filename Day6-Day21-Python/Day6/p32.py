class Calculator:
    def __init__(self, a, b):
        self.a=a
        self.b=b

    def add(self):
        return self.a+self.b

    def sub(self):
        return self.a-self.b

    def mul(self):
        return self.a*self.b
    def div(self):
        return round(self.a/self.b, 2)

c1=Calculator(10, 6)
print("Addition: ",c1.add())
print("Substraction: ",c1.sub())
print("Multiplication: ",c1.mul())
print("Division: ",c1.div())
