class Student:
    college_name="ITM"
    college_code="26010"
    @classmethod
    def m1(cls):
        Student.college_name="IIT"
        cls.college_code="26020"
        
s1=Student()
print("Before:")
print(Student.college_name)
print(Student.college_code)

s1.m1()
print("After:")
print(Student.college_name)
print(Student.college_code)
