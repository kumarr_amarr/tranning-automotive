class Student:
    def __init__(self, name, roll):
        Student.college_name="ITM"
        self.name=name
        self.roll=roll
        
s1=Student("Raj", 101)
print(s1.college_name,s1.name, s1.roll, id(s1.college_name))

s2=Student("Amar", 102)
print(s2.college_name, s2.name, s2.roll, id(s2.college_name))
