"""
Given numbers = range(20), produce a list containing the word 'even' if a number in the numbers is even,
and the word 'odd' if the number is odd.  
Result would look like ['odd','odd', 'even']
"""
print(["even" if number%2==0 else "odd" for number in range(20)])
