"""
Find the common numbers in two lists (without using a tuple or set) 
list_a = [1, 2, 3, 4]
list_b = [2, 3, 4, 5]
"""
list_a = [1, 2, 3, 4]
list_b = [2, 3, 4, 5]

print([ i for i in list_a if i in list_b ])
