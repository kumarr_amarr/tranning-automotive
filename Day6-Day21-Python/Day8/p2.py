class Parent(metaclass=ABCMeta):
    def __init__(self): 
        self.__num=100
    @abstractmethod
    def show(self):
        pass
    
class Child(Parent):  
    def __init__(self):
        super().__init__()
        self.var=10
 
obj=Child()  
obj.show()
