def find_average(mark_list):
    total=0
    try:
        for i in range(0, len(mark_list)):
            total=total+mark_list[i]
        print(total/len(mark_list))
    except TypeError:
        print("Type error occured.")
    except ZeroDivisionError:
        print("Zero Division Error occured.")
    except IndexError:
        print("Index error occured.")
    except:
        print("Some error occured.")
        
try:
    m_list=["1",2,3,4]
    find_average(m_list)
except ValueError:
        print("Value error occured.")
except NameError:
     print("Name error occured.")
except:
        print("Some error occured.")

