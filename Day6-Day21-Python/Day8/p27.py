"""
Produce a list of tuples consisting of only the matching numbers in these lists 
list_a = [1, 2, 3,4,5,6,7,8,9]
list_b = [2, 7, 1, 12].  
Result would look like (4,4), (12,12)
"""
#Doubt
list_a = [1, 2, 3,4,5,6,7,8,9]
list_b = [2, 7, 1, 12]
print([((i,i),(j, j)) for i,j in zip(list_a, list_b) if i not in list_b and j not in list_a])
print([(num, num) for num in list_b if num in list_a])
