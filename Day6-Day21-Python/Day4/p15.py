class Mobile:
    def __init__(self, brand, price):
        print ("Mobile created with id", id(self))
        self.brand=brand
        self.price=price
        
    def return_product(self):
        print ("Mobile being returned has id", id(self))
        print ("Brand being returned is ",self.brand," and price is ",self.price)

print("Mobile-1:")
m1=Mobile("Apple", 10000)
Mobile.return_product(m1)

print("Mobile-2:")
mob2=Mobile("Vivo", 9000)
Mobile.return_product(mob2)
