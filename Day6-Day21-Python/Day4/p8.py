class Mobile():
    def __init__(self, brand, price):
        self.brand=brand
        self.price=price

m1=Mobile("Apple", 20000)
m2=Mobile("Samsung", 3000)

print("Mobile 1 has brand", m1.brand,"and price",m1.price)
print("Mobile 2 has brand", m2.brand,"and price",m2.price)
