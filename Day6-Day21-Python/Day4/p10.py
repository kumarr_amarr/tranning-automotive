class Mobile:
    def __init__(self, brand, price):
          print("Id of self in constructor", id(self))
          self.brand=brand
          self.price=price

mob1=Mobile("Apple", 1000)
print("Id of mob1 in driver code", id(mob1))
mob2=Mobile("Apple", 1000)
print("Id of mob2 in driver code", id(mob2))
