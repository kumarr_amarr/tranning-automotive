class Mobile:
    def __init__(self, brand, price):
        self.brand=brand
        self.price=price
    def display(self):
        print("This mobile has brand", self.brand, "and price", self.price)
        
    def purchase(self):
         print("Purchasing a mobile")
         self.display()

print("Mobile-1:")
m1=Mobile("Apple", 10000)
m1.purchase()

print("Mobile-2:")
mob2=Mobile("Vivo", 9000)
mob2.purchase()
