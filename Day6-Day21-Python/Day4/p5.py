class Mobile:
    def __init__(self, brand, model, price):
        self.brand=brand
        self.model=model
        self.price=price
        
m1=Mobile("Apple", "41-Plus", 60000)
m2=Mobile("Samsung", "A-53", 20000)

print(m1.brand, m1.model, m1.price)
print(m2.brand, m2.model, m2.price)

m1.brand="Vivo"
m1.model="v-29"
m1.price="35000"

print(m1.brand, m1.model, m1.price)

m1.color="White"
print(m1.brand, m1.model, m1.price, m1.color)
