class Mobile:
    def __init__(self):
        print("Inside constructor")
        print("Inside constructor:", id(self))

    def purchase(self):
         print("Purchasing a mobile")
         print("Inside purchase:", id(self))

m1=Mobile()
m1.purchase()
m2=Mobile()
m2.purchase()
