class Mobile:
    def __init__(self, brand, price):
        print ("Mobile created with id", id(self))
        self.brand=brand
        self.price=price
        
    def return_product(self):
        print ("Mobile being returned has id", id(self))
        print ("Brand being returned is ",self.brand," and price is ",self.price)

mob1=Mobile("Apple", 10000)
mob2=mob1
mob2=Mobile("Samsung", 12000)
mob2.price=15000
print(mob1.price)
print(mob2.price)
print ("Id of object referred by mob1 reference variable is :", id(mob1))
print ("Id of object referred by mob2 reference variable is :", id(mob2))

