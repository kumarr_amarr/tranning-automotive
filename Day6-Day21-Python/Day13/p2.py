def fibonacci_series(n):
    a = 0
    b = 1
    for _ in range(n):
        yield a
        a, b = b, a + b

fs = fibonacci_series(10)
for num in fs:
    print(num, end= "  ")
