"""
Write a program to find and display the product of three positive integer values based on the rule mentioned below:
It should display the product of the three values except when one of the integer value is 7.
In that case, 7 should not be included in the product and the values to its left also should not be included.
If there is only one value to be considered, display that value itself. If no values can be included in the product, display -1.
Note: Assume that if 7 is one of the positive integer values, then it will occur only once. Refer the sample I/O given below.

"""
product=lambda n1, n2, n3:n1*n2*n3 if (n1!=7 and n2!=7 and n3!=7) else n2*n3 if n1==7 else n3 if n2==7 else -1
print(product(1,5,3))
print(product(3,7,8))
print(product(7,4,3))
print(product(1,5,7))
