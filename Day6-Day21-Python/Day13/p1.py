class InvalidAgeEException(Exception):
    pass
class InvalidJobProfileException(Exception):
    pass
class InvalidNameException(Exception):
    pass

class Applicant:
    def __init__(self, name, jobProfile, age):
        self.__name=name
        self.__jobProfile=jobProfile
        self.__age=age

    def getName(self):
        return self.__name

    def setName(self, name):
        self.__name=name

    def getJobProfile(self):
        return self.__jobProfile

    def setJobProfile(self, jobProfile):
        self.__jobProfile=jobProfile

    def getAge(self):
        return self.__age
    
    def setAge(self, age):
        self.__name=age


class Validator:
    @staticmethod
    def validateName(name):
        if name.strip() != "" and name.strip() != None:
            return True
        else:
            return False

    @staticmethod
    def validateJobProfile(jobProfile):
        if jobProfile =="Associate" or jobProfile =="Clerk" or jobProfile =="Executive" or jobProfile =="Officer":
            return True
        else:
            return False

    @staticmethod
    def validateAge(age):
        if age>=18 and age<=30:
            return True
        else:
            return False

    @staticmethod
    def validate(applicant):
        if not Validator.validateName(applicant.getName()):
            raise InvalidNameException("Invalid name")
        
        if not Validator.validateAge(applicant.getAge()):
            raise InvalidAgeEException("Invalid age")

        if not Validator.validateJobProfile(applicant.getJobProfile()):
            raise InvalidJobProfileException("Invalid job profile")
        print("Application submitted successfully!")

def main():
    applicant=Applicant(" ", "Associate", 19)
    try:
        Validator.validate(applicant)
    except InvalidNameException as e:
        print(e)

    except InvalidAgeException as e:
        print(e)
    except InvalidJobProfileException as e:
        print(e)

if __name__=="__main__":
    main()
    
