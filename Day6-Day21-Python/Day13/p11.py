"""
Implement a program to find the area of a circle by using the formula given below and display the calculated area. 
Area = pi*radius*radius 
The value of pi is 3.14.

"""
import math
area_of_circle=lambda r:3.14*r**2
print(area_of_circle(10))
print(area_of_circle(4))
