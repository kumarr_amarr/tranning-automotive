"""
Write a java function to check whether three given numbers can form the sides of a triangle. 
Hint: Three numbers can be the sides of a triangle if none of the numbers are greater than or equal to the sum of the other two numbers.


"""
isTriangle=lambda s1, s2, s3:True if  s1<s2+s3 and s2<s1+s3 and s3<s1+s2 else False
print(isTriangle(2,2,3))
