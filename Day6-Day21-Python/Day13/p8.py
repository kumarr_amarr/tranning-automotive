"""
Implement a program to find and display the maximum number out of the given three numbers.

"""
import math
max_number=lambda a, b, c:max([a,b,c])
print(max_number(1,4,4))
print(max_number(1,4,6))
print(max_number(1,-7,10))
