"""
Implement a program to find and display the maximum number out of the given three numbers.

"""
import math
max_number=lambda a, b, c:max([a,b,c])
print(max_number(3,4,1))
print(max_number(4,4,1))
print(max_number(1,-7,10))
