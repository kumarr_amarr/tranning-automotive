"""
Implement a program to convert temperature from Fahrenheit to Celsius degree by using the formula given below and display the converted value. 
C = ((F-32)/9)*5 where, C represents temperature in Celsius and F represents temperature in Fahrenheit.

"""
import math
fahrenheit_to_celsius=lambda F:((F-32)/9)*5
print(fahrenheit_to_celsius(32))
print(fahrenheit_to_celsius(50))
