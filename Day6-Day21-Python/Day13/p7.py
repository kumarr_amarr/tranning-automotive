"""
Quadratic equation is an equation with degree 2 in the form of ax2 +bx + c = 0 where a, b and c are the coefficients.
Implement a program to solve a quadratic equation.
Find the discriminant value using the formula given below.
discriminant = b2 - 4ac
If the discriminant is 0, the values of both the roots will be same. Display the value of the root.
If the discriminant is greater than 0, the roots will be unequal real roots. Display the values of both the roots.
If the discriminant is less than 0, there will be no real roots. Display the message "The equation has no real root"
Use the formula given below to find the roots of a quadratic equation.
x = (-b ± discriminant)/2a

"""
import math
roots_of_quadratic_equation=lambda a, b, c:f"The root is {-b/2*a}" if (b**2-4*a*c) ==0 else f"The roots are {(-b + math.sqrt((b**2-4*a*c)))/(2*a)} and {(-b - math.sqrt((b**2-4*a*c)))/(2*a)}" if (b**2-4*a*c) >0 else "The equation has no real roots"
print(roots_of_quadratic_equation(1,4,4))
print(roots_of_quadratic_equation(1,4,6))
print(roots_of_quadratic_equation(1,-7,10))
