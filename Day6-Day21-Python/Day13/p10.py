"""
Implement a program that displays a message for a given number based on the below conditions.
If the number is a multiple of 3, display "Zip".
If the number is a multiple of 5, display "Zap".
If the number is a multiple of both 3 and 5, display "Zoom",
For all other cases, display "Invalid".


"""
import math
zip_zap_zoom=lambda n:"Zoom" if n%5==0 and n%3==0 else "Zap" if n%5==0 else "Zip" if n%3==0 else "Invalid"
print(zip_zap_zoom(10))
print(zip_zap_zoom(15))
print(zip_zap_zoom(11))
