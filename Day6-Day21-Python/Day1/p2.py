"""
Write a python program to find and display the product of three positive integer values based on the rule mentioned below:

It should display the product of the three values except when one of the integer value is 7. In that case, 7 should not
be included in the product and the values to its left also should not be included.
If there is only one value to be considered, display that value itself. If no values can be included in the product, display -1.

Note: Assume that if 7 is one of the positive integer values, then it will occur only once. Refer the sample I/O given below.
Sample Input	Expected Output
1, 5, 3	            15
3, 7, 8	            8
7, 4, 3	            12
1, 5, 7	            -1

"""

n1, n2, n3 =1,5,3
n1, n2, n3 =3, 7, 8
n1, n2, n3 =7, 4, 3
n1, n2, n3 =1, 5, 7
n1, n2, n3 =1, -5, 7
n1, n2, n3 =-1, 5, 7
n1, n2, n3 =1, 5, -7

product=1
if n1>=1 and n2>=1 and n3>=1:

    if n1 != 7 and n2 != 7  and n3 != 7:
        product = n1* n2 * n3
    elif n1==7 :
        product = n2 * n3
    elif n2 ==7 :
        product = n3
    elif n3 == 7:
        product = -1
    print(product)
else:
    print("-ve found")
