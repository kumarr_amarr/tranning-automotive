"""
Write a Python program that converts temperature in Celsius to Fahrenheit and vice versa. Implement the requirements using functions.
Hint: 0C= (0F-32)*(5/9)

"""

def celsius_to_fahrenheit(c):

    f=9/5*c+32
    return f

def fahrenheit_to_celsius(f):

    c=(f-32)*(5/9)
    return c

print(celsius_to_fahrenheit(37))
print(fahrenheit_to_celsius(98.60000000000001))
