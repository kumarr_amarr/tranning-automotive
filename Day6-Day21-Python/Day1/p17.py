""" checks if a number is a Buzz number or not. A number is a Buzz
number if it ends with 7 or is divisible by 7. 63 is a Buzz number as it
is divisible by 7. 747 is also a Buzz number as it ends with 7. 83 is
not a Buzz number as it is neither divisible by 7 nor it ends in 7. """


def check_buzz_number(number):
    last_digit = number%10
    if number % 7==0 or last_digit==7:
        return "Buzz."
    else:
        return "Not Buzz."

print(check_buzz_number(67))
