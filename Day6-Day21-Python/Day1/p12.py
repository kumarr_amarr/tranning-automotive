def check_value(message, num):
    msg=message[:num]
    return len(msg)

result=check_value("Infosys",3)
print("Result:", result)
#result=check_value(4,'Infosys‘) #arg error
#print("Result:", result)
result=check_value('Infosys',4)
print("Result:", result)
result=check_value('Infosys',len('Infosys'))
print("Result:", result)
