"""
Write a python program to input basic salary of an employee and calculate its Gross salary according to following:
Basic Salary <= 10000 : HRA = 20%, DA = 80%
Basic Salary <= 20000 : HRA = 25%, DA = 90%
Basic Salary > 20000 : HRA = 30%, DA = 95%

"""

b_sal=float(input("Enter Besic Salary:"))
HRA=0
DA=0
if b_sal <=10000:
    HRA=(20/100)*b_sal
    DA=(80/100)*b_sal
elif b_sal >10000 and b_sal <= 20000:
    HRA=(25/100)*b_sal
    DA=(90/100)*b_sal
elif b_sal >20000:
    HRA=(30/100)*b_sal
    DA=(95/100)*b_sal
    
g_sal=b_sal+HRA+DA
print("Gross Salary is",g_sal)

