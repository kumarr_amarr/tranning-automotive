"""
The flight ticket rates for a round-trip (Mumbai->Dubai) were as follows: 
Rate per Adult: Rs. 37550.0 
Rate per Child: 1/3rd of the rate per adult 
Service Tax: 7% of the ticket amount (including all passengers) 
As it was a holiday season, the airline also offered 10% discount on the final ticket cost (after inclusion of the service tax).
Find and display the total ticket cost for a group which had adults and children.

Test the program with different input values for number of adults and children.
 
Sample Input                                    Expected Output
Number of adults    Number of children	 
5	                    2	            Total Ticket Cost: 204910.35
3                           1	            Total Ticket Cost: 120535.5



"""

def calculate_total_ticket_cost(no_of_adults, no_of_children):
    adult_ticket_cost=no_of_adults*37550
    child_ticket_cost=no_of_children*(37550*1/3)
    total_ticket_cost=(adult_ticket_cost+child_ticket_cost)-(adult_ticket_cost+child_ticket_cost)*0.03
    #Write your logic here
 
    return total_ticket_cost
 
 
#Provide different values for no_of_adults, no_of_children and test your program
total_ticket_cost=calculate_total_ticket_cost(5,2)
print("Total Ticket Cost:",total_ticket_cost)
total_ticket_cost=calculate_total_ticket_cost(3,1)
print("Total Ticket Cost:",total_ticket_cost)


def calculate_total_ticket_cost(no_of_adults, no_of_children):
    # Given rates and constants
    rate_per_adult = 37550.0
    rate_per_child = rate_per_adult / 3
    service_tax_rate = 0.07
    discount_rate = 0.10
    total_adult_cost = no_of_adults * rate_per_adult
    total_child_cost = no_of_children * rate_per_child
    subtotal = total_adult_cost + total_child_cost
    service_tax = subtotal * service_tax_rate
    total_cost_before_discount = subtotal + service_tax
    total_cost = total_cost_before_discount * (1 - discount_rate)
    return total_cost

total_ticket_cost=calculate_total_ticket_cost(5,2)
print("Total Ticket Cost:",total_ticket_cost)
total_ticket_cost=calculate_total_ticket_cost(3,1)
print("Total Ticket Cost:",round(total_ticket_cost,2))
