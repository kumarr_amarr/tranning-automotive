""" You have x no. of 5 rupee coins and y no. of 1 rupee coins. You want to purchase an item for amount z.
The shopkeeper wants you to provide exact change. You want to pay using minimum number of coins.
How many 5 rupee coins and 1 rupee coins will you use? If exact change is not possible then display -1.
 
Sample Input  Expected Output


Available Rs. 1 coins
	
Available Rs. 5 notes
	
Amount to be made
	
Rs. 1 coins needed
	
Rs. 5 notes needed


2
	
4
	
21
	
1
	
4


11
	
2
	
11
	
1
	
2


3
	
3
	
19
	

-1 """


def coin_exchange(amount_to_make, number_of_one_rupee_coin, number_of_five_rupee_coin):
    total_amount = 1*number_of_one_rupee_coin + 5*number_of_five_rupee_coin
    if amount_to_make > total_amount:
        return -1

    max_five_rupee_coin = min(amount_to_make // 5, number_of_five_rupee_coin)

    while max_five_rupee_coin >= 0:
        remaining_amount = amount_to_make - 5*max_five_rupee_coin
        if remaining_amount <= number_of_one_rupee_coin:
            return (max_five_rupee_coin, remaining_amount)
        max_five_rupee_coin -= 1
    return -1

result = coin_exchange(21, 2, 4)
if result == -1:
    print(result)
else:
    print("RS. 5 coins needed:", result[0])
    print("RS. 1 coins needed:", result[1])


        









    
