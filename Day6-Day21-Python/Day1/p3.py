"""
Write a python function to check whether three given numbers can form the sides of a triangle. 
Hint
: Three numbers can be the sides of a triangle if none of the numbers are greater than or equal to the sum of the other two numbers.


"""

def is_valid_Side(s1, s2, s3):
    if s1<s2+s3 and s2<s1+s3 and s3<s1+s2:
        return True
    else:
        return False

n1, n2, n3 =13,5,12
n1, n2, n3 =1,5,9
n1, n2, n3 =13,5,-12
print(is_valid_Side(n1, n2, n3))


