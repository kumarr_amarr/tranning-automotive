"""
Write a python function, find_correct() which accepts a dictionary and returns a list as per the rules mentioned below.
The input dictionary will contain correct spelling of a word as key and the spelling provided by a contestant as the value.

The function should identify the degree of correctness as mentioned below:
CORRECT, if it is an exact match

ALMOST CORRECT, if no more than 2 letters are wrong
WRONG, if more than 2 letters are wrong or if length (correct spelling versus spelling given by contestant) mismatches.

and return a list containing the number of CORRECT answers, number of ALMOST CORRECT answers and number of WRONG answers. 
Assume that the words contain only uppercase letters and the maximum word length is 10.
Sample Input	Expected Output
{"THEIR": "THEIR", "BUSINESS": "BISINESS","WINDOWS":"WINDMILL","WERE":"WEAR","SAMPLE":"SAMPLE"}	[2, 2, 1]

"""
def find_correct(d):
    list1=[]
    correct=0
    almost_correct=0
    wrong=0
    for k, v in d.items():
        mismatch_char=0
        for i in range(len(k)):
            if k[i] != v[i]:
                mismatch_char+=1
            
        if mismatch_char==0:
            correct+=1
        elif mismatch_char <= 2:
            almost_correct+=1
        else:
            wrong+=1
    list1=[correct, almost_correct, wrong]
    return list1

d={"THEIR": "THEIR", "BUSINESS": "BISINESS","WINDOWS":"WINDMILL","WERE":"WEAR","SAMPLE":"SAMPLE"}
print(find_correct(d))
