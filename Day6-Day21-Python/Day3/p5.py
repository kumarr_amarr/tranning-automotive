"""
Write a Python program to sum all the items in a dictionary."""
def sum_of_dict_values(d):
    value=d.values()
    sum_res=0
    for v in value:
        sum_res+=v
    return sum_res
dict1={1:10, 2:20, 3:30}
print(sum_of_dict_values(dict1))
