"""
Care hospital wants to know the medical speciality visited by the maximum number of patients.
Assume that the patient id of the patient along with the medical speciality visited by the patient is stored in a list.
The details of the medical specialities are stored in a dictionary as follows:
{
"P":"Pediatrics",
"O":"Orthopedics",
"E":"ENT
} 

Write a function to find the medical speciality visited by the maximum number of patients and return the name of the speciality.

Note: 
1.	Assume that there is always only one medical speciality which is visited by maximum number of patients.
2.	Perform case sensitive string comparison wherever necessary.
Sample Input	Expected Output
[101,P,102,O,302,P,305,P]	Pediatrics
[101,O,102,O,302,P,305,E,401,O,656,O]	Orthopedics
[101,O,102,E,302,P,305,P,401,E,656,O,987,E]	ENT

"""
def max_visited_medical_speciality(l):
    medical_specialities_details= {"P":"Pediatrics","O":"Orthopedics","E":"ENT"}
    visited_list=[]
    medical_specialities_details_list=[]
    for element in l:
        if type(element)==int:
            visited_list.append(element)
        else:
            medical_specialities_details_list.append(element)
    max_visit=max(visited_list)
    for i in range(len(visited_list)):
        if visited_list[i]==max_visit:
            index_of_max_visit=i
            res=medical_specialities_details[medical_specialities_details_list[index_of_max_visit]]
    return res
    
list1=[101,"P",102,"O",302,"P",305,"P"]
print(max_visited_medical_speciality(list1))
list1=[101,"O",102,"O",302,"P",305,"E",401,"O",656,"O"]
print(max_visited_medical_speciality(list1))
list1=[101,"O",102,"E",302,"P",305,"P",401,"E",656,"O",987,"E"]
print(max_visited_medical_speciality(list1))
