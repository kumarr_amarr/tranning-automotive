import sys #sys module has to read the command line arguments

def gcd(n1, n2):
    while n2:
        n1, n2 = n2, n1 % n2
    return n1
 
    
number1=int(sys.argv[1])
number2=int(sys.argv[2])
print(gcd(number1, number2))



    
