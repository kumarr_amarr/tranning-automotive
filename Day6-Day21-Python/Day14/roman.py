def roman_number(num):
    numbers=[1, 4, 5, 9, 10, 40, 50, 90, 100, 400, 500, 900, 1000]
    roman_symbols=["I", "IV", "V", "IX", "X", "XL", "L", "XC", "C", "CD", "D", "CM", "M"]
    roman_num =""
    i=len(numbers) - 1
    while num>0 and num<5000:
        for j in range(num//numbers[i]):
            roman_num=roman_num+roman_symbols[i]
            num= num-numbers[i]
        i -= 1
    return roman_num

number = 2001
print(roman_number(number))
