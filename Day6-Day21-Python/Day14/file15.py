"""
Write a Python program to count the frequency of words in a file.
"""
file=open("file.txt", "r")
list_of_words=file.read().split()
lst={word:list_of_words.count(word) for word in list_of_words}
print(lst)
file.close()
