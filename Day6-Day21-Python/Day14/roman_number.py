def roman_number(num):
    numbers=[1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1]
    roman_symbols=["M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"]
    roman_num = ''
    i=0
    while num>0 and num<5000:
        for j in range(num//numbers[i]):
            roman_num=roman_num+roman_symbols[i]
            num=num-numbers[i]
        i+=1
    return roman_num

number =9
print(roman_number(number))
