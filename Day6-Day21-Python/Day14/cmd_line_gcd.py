import sys #sys module has to read the command line arguments

def gcd(n1, n2):
    factors_of_n1=[]
    factors_of_n2=[]
    for i in range(1,n1+1):
        if n1%i==0:
            factors_of_n1.append(i)
            
    for j in range(1,n2+1):
        if n2%j==0:
            factors_of_n2.append(j)

    if len(factors_of_n1)==len(factors_of_n2):
        return factors_of_n1[-1]
    
    elif len(factors_of_n1)<len(factors_of_n2):
        factors_of_n2=factors_of_n2[:len(factors_of_n1)]
        if factors_of_n1[-1]<factors_of_n2[1]:
            return factors_of_n1[-1]
        else:
            return factors_of_n2[-1]
        
    else:
        factors_of_n1=factors_of_n1[:len(factors_of_n2)]
        if factors_of_n1[-1]<factors_of_n2[1]:
            return factors_of_n1[-1]
        else:
            return factors_of_n2[-1]
    
        
    
number1=int(sys.argv[1])
number2=int(sys.argv[2])
print(gcd(number1, number2))



    
