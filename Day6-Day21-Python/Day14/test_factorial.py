import pytest
import factorial

@pytest.mark.fact
def test_factorial():
    fact=factorial.factorial(5)
    expected=120
    assert fact==expected
    
