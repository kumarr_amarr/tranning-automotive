"""
Write a Python program to combine each line from first file with the corresponding line in second file.
"""
#doubt
file1=open("file.txt", "r")
file2=open("file1.txt", "r")
file3=open("file2.txt", "w")

for f1_line, f2_line in zip(file1.readlines(),file2.readlines()):
    file3.write(f1_line+f2_line)
print("Successfully written.")

file1.close()
file2.close()
file3.close()
