import csv
try:
    with open("Student.csv", "w") as file:
        csv_writer=csv.writer(file)
        csv_writer.writerow(["Roll", "Name", "Course"])
        csv_writer.writerow(["101", "Amar", "Python"])
        print("Data written successfully.")
    
except FileNotFoundError as e:
    print(e)
