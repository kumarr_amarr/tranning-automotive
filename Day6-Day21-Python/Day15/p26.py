"""
Write a python function which accepts three numbers and returns True if
a. one of the three numbers is "close" to any one of the other numbers (differing from a number by at most 1), and
b.Number that is left out is "far", differing from both other values by 2 or more.
Return false if the above mentioned conditions are not satisfied.

"""
def is_closed_number(n1, n2, n3):
    if (abs(n1-n2)<=1 and abs(n1-n3)>=2 and abs(n2-n3)>=2):
        return True
    if (abs(n1-n3)<=1 and abs(n1-n2)>=2 and abs(n3-n2)>=2):
        return True
    if (abs(n2-n3)<=1 and abs(n2- n1)>= 2 and abs(n3-n1)>=2):
        return True
    return False

print(is_closed_number(4,1,3))
print(is_closed_number(5,6,7))
