
from abc import ABCMeta,abstractmethod
class Parent(metaclass=ABCMeta):
    @abstractmethod
    def sample(self): pass
class Child(Parent):
    pass
class GrandChild(Child):
    def sample(self): pass
class GreatGrandChild(GrandChild):
    pass
g=GrandChild()
