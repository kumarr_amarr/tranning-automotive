import csv
h_row=["Roll", "Name", "Course"]
d_row=[
            [102, "Raj", "Java"],
            [103, "John", "AWS"],   
            [104, "Hansh", "Database"]
        ]
try:
    with open("Student.csv", "w") as file:
        csv_writer=csv.writer(file)
        csv_writer.writerow(h_row)
        for row in d_row:
            csv_writer.writerow(row)
        print("Data written successfully.")
except:
    print("File not found.")