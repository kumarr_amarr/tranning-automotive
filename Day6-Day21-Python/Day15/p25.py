"""
Write a Python function to rotate the list of elements by N positions. The function should return the rotated list.
"""
def list_rotation(lst, position_of_rotation):
    while position_of_rotation>len(lst):
        position_of_rotation=position_of_rotation%len(lst)
    return lst[position_of_rotation:]+lst[:position_of_rotation]
        
lst=[1,2,3,4,5]
print(list_rotation(lst, 9))
