"""
Write a python function to identify and return the number name of a given number.
The number should be in the range 1 to 1000. If the number is invalid, return -1.
"""

def number_to_word(num):
    numbers=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,30,40,
             50,60,70,80,90,100,200,300,400,500,600,700,800,900,1000
             ]
    numbers_speel=["one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten",
                  "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen",
                  "eighteen", "nineteen", "twenty", "thirty", "fourty", "fifty", "sixty", "seventy",
                  "eighty", "ninety", "one hundred", "two hundred", "three hundred", "four hundred",
                  "five hundred", "six hundred", "seven hundred", "eight hundred", "nine hundred", "one thousand"
                  ]
   
    number_spelling=""
    if num >0 and num<=1000:
        i=len(numbers)-1
        
        while num>0:
            for j in range(num//numbers[i]):
        
                number_spelling=number_spelling+" "+"".join(numbers_speel[i])
                num=num-numbers[i]
            i=i-1
            
        return number_spelling

    else:
        print(-1)

print(number_to_word(51))
