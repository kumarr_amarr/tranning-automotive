class Test1(Exception):
    pass
class Test2:
    def sample(self):
        try:
            raise Test1
        except Test1:
            print(1)
        finally:
            print(2)
try:
    Test2().sample()
except Test1:
    print(3)
finally:
    print(4)
