"""
the checkNameValidity() method.
The length of the name should be between 2 and 30 characters (both inclusive)
The name can contain only alphabets and spaces
The first character of each word of the name should be an upper case alphabet
Each word should be separated by a space
The name should not start or end with a space
Special characters should not be allowed
Return true if the name is valid, else return false.
 
Test the functionalities using the main method of the Tester class.
"""
import re

class Tester:
    @staticmethod
    def checkNameValidity(name):
        if len(name) < 2 or len(name) > 30:
            return False
        pattern = r'^[A-Z][a-z]*(?:\s[A-Z][a-z]*)*$'
        
        if re.match(pattern, name):
            return True
        else:
            return False

def main():
    test_cases = [
        "Roger Federer",
        "roger federer"         
    ]
    
    for name in test_cases:
        if Tester.checkNameValidity(name):
            print("The name is valid!")
        else:
            print("The name is invalid!")

if __name__ == "__main__":
    main()
