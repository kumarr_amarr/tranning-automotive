from functools import reduce

class Product:
    def __init__(self, id, name, price):
        self.__id = id
        self.__name = name
        self.__price = price

    def get_id(self):
        return self.__id
    
    def get_name(self):
        return self.__name
    
    def get_price(self):
        return self.__price
    
    def __str__(self):
        return f"Product(id={self.__id}, name={self.__name}, price={self.__price})"

    @staticmethod
    def search_product_less_than_price(products_list, price):
        filtered_products=filter(lambda product: product.get_price()<price, products_list)
        return list(map(str, filtered_products))

    @staticmethod
    def count_product_less_than_price(products_list, price):
        return len(list(filter(lambda product: product.get_price()<price,products_list)))

    @staticmethod
    def display_product_list(products_list):
        for product in products_list:
            print(product)

    @staticmethod
    def sum_of_all_product_prices(products_list):
        return reduce(lambda sum, product:sum+product.get_price(), products_list, 0)

    @staticmethod
    def display_summary_statistics(products_list):
        prices=list(map(lambda product: product.get_price(), products_list))
        summary = {
            "count": len(prices),
            "min": min(prices) if prices else 0,
            "max": max(prices) if prices else 0,
            "sum": reduce(lambda sum, price: sum+price, prices, 0),
            "average": reduce(lambda sum, price: sum+price, prices, 0)/len(prices) if prices else 0
        }
        return summary

    @staticmethod
    def max_price_product_details(products_list):
        if not products_list:
            return None
        return reduce(lambda max_product, product: product if product.get_price()>max_product.get_price() else max_product, products_list)

    @staticmethod
    def min_price_product_details(products_list):
        if not products_list:
            return None
        return reduce(lambda min_product, product: product if product.get_price() < min_product.get_price() else min_product, products_list)

    @staticmethod
    def display_product_name_along_price(products_list):
        list(map(lambda product: print(f"{product.get_name()}: {product.get_price()}"), products_list))

    @staticmethod
    def display_list_of_product_names(products_list):
        return list(map(lambda product: product.get_name(), products_list))

products_list = [
    Product(1, "Sony mobile", 25000),
    Product(2, "Lenovo mobile", 15000),
    Product(3, "Nokia mobile", 10000),
    Product(4, "Samsung mobile", 40000),
    Product(5, "Real Me", 50000)
]

print("Products less than 20000:")
print(Product.search_product_less_than_price(products_list, 20000))

print("\nCount of products less than 20000:")
print(Product.count_product_less_than_price(products_list, 20000))

print("\nProduct List:")
Product.display_product_list(products_list)

print("\nSum of all product prices:")
print(Product.sum_of_all_product_prices(products_list))

print("\nSummary statistics of products:")
print(Product.display_summary_statistics(products_list))

print("\nMax price product details:")
print(Product.max_price_product_details(products_list))

print("\nMin price product details:")
print(Product.min_price_product_details(products_list))

print("\nProduct name along with price:")
Product.display_product_name_along_price(products_list)

print("\nList of product names:")
print(Product.display_list_of_product_names(products_list))
