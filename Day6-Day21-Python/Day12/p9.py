#Generator Func

def my_gen(start, end):
    for i in range(start, end+1):
        yield i

for n in my_gen(10, 15):
    print(n)
    

