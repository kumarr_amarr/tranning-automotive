"""
Validating Names
Now let's try validating the names.

Names should contain only alphabets and optional white spaces.
For example, Tom, Tom Jerry, Tom and Jerry

The regex pattern for this would be ([A-Za-z]+\\s?)+
Which means a combination of lowercase alphabets, uppercase alphabets followed by an optional white space. And the same pattern can repeat any number of times to form a group of words.

"""

import re

def validate_name(name):
    pattern = r'^([A-Za-z]+\s?)+$'
    if re.match(pattern, name):
        return True
    else:
        return False

print(validate_name("Tom"))         
print(validate_name("Tom Jerry"))   
print(validate_name("Tom and Jerry"))  
print(validate_name("Tom123"))         
print(validate_name("Tom_Jerry"))
