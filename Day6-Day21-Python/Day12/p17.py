"""
Objective: To understand Regular Expression.
Exercise Description:
Write a Java program that will validate email id  based on the below requirement
Email id must start with a string and can contain numbers.
Email id can have “_” or “.” as a valid character. No other symbol like #, %, &, ( etc. are allowed.
Must contain @ symbol before the domain name.
The domain name could be a combination of alphabets and numbers.
Some of the valid email ids:
Tom_Jerry@infosys.com
Tom1.Jerry@infosys.com
TomJerry1@domain.com
TomJerry@domain.co.in
Some of the invalid email ids:
Tom@Jerry@tom.com
Tom1&Jerry@tom.com
Tom*Jerry

"""
import re

def validate_email(email):
    pattern = r'^[a-zA-Z0-9._]+@[a-zA-Z0-9]+\.[a-zA-Z]{2,}(?:\.[a-zA-Z]{2,})?$'
    if re.match(pattern, email):
        return True
    else:
        return False

def main():
    test_cases = [
        "Tom_Jerry@infosys.com",    
        "Tom1.Jerry@infosys.com",   
        "TomJerry1@domain.com",    
        "TomJerry@domain.co.in",    
        "Tom@Jerry@tom.com",       
        "Tom1&Jerry@tom.com",      
        "Tom*Jerry",                
        "Tom.Jerry@domain",         
        "TomJerry@.com",           
        "TomJerry@domain.c",        
        "TomJerry@domain..com"  
    ]
    
    for email in test_cases:
        print(f"'{email}': {validate_email(email)}")

if __name__ == "__main__":
    main()
