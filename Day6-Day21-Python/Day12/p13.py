"""
Let us assume that the customers should set password which should have 8 to 10 letters followed by 4 digits. Let us validate the password.
Sample passwords: rtysvsjhu1234, fgyfvdhsdb745, bdhfv1


"""
import re

def validate_password(password):
    pattern = re.compile(r'^[A-Za-z]{8,10}\d{4}$')
    if pattern.match(password):
        return True
    else:
        return False

passwords = ["rtysvsjhu1234", "fgyfvdhsdb745", "bdhfv1"]

for pw in passwords:
    if validate_password(pw):
        print("valid")
    else:
        print("invalid")
        
