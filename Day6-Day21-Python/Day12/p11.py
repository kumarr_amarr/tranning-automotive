class OnlinePortal:
    item_list=["Vivo", "Samsung", "Apple", "Oppo"]
    quantity_list=[5, 10, 7, 15]
    price_list=[10000, 15000, 30000, 20000]

    @classmethod
    def search_item(cls, item):
        if item in cls.item_list:
                return cls.item_list.index(item)
        else:
            return -1
    @classmethod
    def place_order(cls, index, emi, quantity):
        cls.quantity_list[index] -= quantity
        
        total_price= quantity * cls.price_list[index]
        final_price=total_price
        
        if emi>0:
            return total_price+total_price*.02
        else:
            emi=1
            return total_price-total_price*.02

    @classmethod
    def add_stock(cls, item_name, quantity):
        for item in cls.item_list:
            if item == item_name:
                index_of_item=cls.item_list.index(item)
                if cls.quantity_list[index_of_item]<=10:
                    cls.quantity_list[index_of_item]+=quantity
                    return "Stock Added Successfully."
                else:
                    return -1

    @classmethod
    def add_item(cls, item_name, price, quentity):
        if item_name not in cls.item_list:
            cls.item_list.append(item_name)
            cls.quantity_list.append(quentity)
            cls.price_list.append(price)
            return "Item Added Successfully."
        else:
            return -2
            
            
class Buyer:
    def __init__(self, name, email_id):
        self.name = name
        self.email_id = email_id

    def purchase(self, item_name, quentity, emi):
        self.item_name=item_name
        self.quentity=quentity
        self.emi=emi
        if self.item_name in OnlinePortal.item_list:
            index_of_item=OnlinePortal.item_list.index(self.item_name)
            if OnlinePortal.quantity_list[index_of_item]>=self.quentity:
                return OnlinePortal.place_order(index_of_item, self.emi, self.quentity)
            else:
                return -1
        return -2
                

buyer1=Buyer("Amar", "amar@gmail.com")
print(buyer1.purchase("Samsung", 2, 3))
print(OnlinePortal.search_item("Apple"))
print(OnlinePortal.add_stock("Apple", 5))
print(OnlinePortal.add_item("Nokia", 17000, 30))
print(OnlinePortal.item_list)
print(OnlinePortal.quantity_list)
print(OnlinePortal.price_list)



    
