
#Doubt
class Consultant:
    def __init__(self, name, registered_company_list, vacancy_list, registered_student_dict):
        self.__name = name
        self.__registered_company_list = registered_company_list
        self.__vacancy_list = vacancy_list
        self.__registered_student_dict = registered_student_dict

    def get_name(self):
        return self.__name
    
    def get_registered_company_list(self):
        return self.__registered_company_list
    
    def get_vacancy_list(self):
        return self.__vacancy_list
    
    def get_registered_student_dict(self):
        return self.__registered_student_dict

    def validate_vacancy(self, company_name):
        for i in range(len(self.__registered_company_list)):
            if self.__registered_company_list[i].get_name() == company_name and self.__vacancy_list[i] > 0:
                return i
        return -1
    
    def register_student_for_placement(self, index, student_id):
        company_name=self.__registered_company_list[index].get_name()
        print(company_name,"Debug")
        self.__vacancy_list[index] -=1
        
        if company_name in self.__registered_student_dict:
            self.__registered_student_dict[company_name] = student_id

class Student:
    def __init__(self, name, student_id, branch, aggregate_percentage, year_of_passing):
        self.__name = name
        self.__student_id = student_id
        self.__branch = branch
        self.__aggregate_percentage = aggregate_percentage
        self.__year_of_passing = year_of_passing

    def get_name(self):
        return self.__name
    
    def get_student_id(self):
        return self.__student_id
    
    def get_branch(self):
        return self.__branch
    
    def get_aggregate_percentage(self):
        return self.__aggregate_percentage
    
    def get_year_of_passing(self):
        return self.__year_of_passing
    
    def check_eligibility(self):
        if self.__aggregate_percentage>=65 and self.__year_of_passing >=2015:
            return True
        return False

    def apply_for_job(self, company_name, consultant):
        vacancy_index = consultant.validate_vacancy(company_name)
        if vacancy_index !=-1:
            if self.check_eligibility():
                consultant.register_student_for_placement(vacancy_index, self.__student_id)
            else:
                return -1
        else:
            return -1

class RegisteredCompany:
    def __init__(self, name, list_of_vacancy):
        self.__name = name
        self.__list_of_vacancy = list_of_vacancy

    def get_name(self):
        return self.__name
    
    def get_list_of_vacancy(self):
        return self.__list_of_vacancy


Wipro=RegisteredCompany("Wipro", ["Python Developer", "Cloud Engineer"])
Infosys=RegisteredCompany("Infosys", ["Java Developer", "Front-end Developer"])

student1= Student("Amar", 101, "ABC", 72.38, 2021)
student2 =Student("John", 102, "XYZ", 70, 2022)

consultant_name = "RPS"
registered_company_list= [Wipro, Infosys]
vacancy_list= [len(company.get_list_of_vacancy()) for company in registered_company_list]
registered_student_dict={}

consultant= Consultant(consultant_name, registered_company_list, vacancy_list, registered_student_dict)

print(consultant.get_name())
print([company.get_name() for company in consultant.get_registered_company_list()])
print(consultant.get_vacancy_list())
print(consultant.get_registered_student_dict())

student1.apply_for_job("Wipro", consultant)
print(consultant.get_vacancy_list())
print(consultant.get_registered_student_dict())

student2.apply_for_job("Infosys", consultant)
print(consultant.get_vacancy_list())
print(consultant.get_registered_student_dict())
