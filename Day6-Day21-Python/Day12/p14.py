"""
Now let's try to validate mobile numbers using a regular expression.
The conditions are:
It should be a 10 digit number
Hyphens should come after the 3rd and the 6th digits
Examples: 881-123-3333, 881-123-3334, 881-123-3335
First, let's create a regex pattern.
We need 3 digits in the beginning. A pattern for that could be \\d{3}
This is followed by a hyphen and another 3 digits, i.e. \\d{3}-\\d{3}
Following the same approach, the final regex pattern will be \\d{3}-\\d{3}-\\d{4}

"""

import re

def validate_mobile_number(number):
    pattern = r'^\d{3}-\d{3}-\d{4}$'
    if re.match(pattern, number):
        return True
    else:
        return False

numbers = ["881-123-3333", "881-123-3334", "881-123-3335", "881-123-333"]
for number in numbers:
    print(f"{number}: {validate_mobile_number(number)}")
