from flask import Flask

app=Flask(__name__)

@app.route("/")
def welcome():
    return "Hello world"

@app.route("/sq/<int:n>")
def square(n):
    return n*n

@app.route('/user/<int:id>')
def get_user(id):
    print(id)
    return '<h2>data for user</h2>'


import controllers.user_controller as user_controller