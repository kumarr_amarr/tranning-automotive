from app import app
from model.user_model import user_model
from flask import request
obj=user_model()

@app.route("/user/singup")
def singup():
    return "This is singup operation"

@app.route("/sign")
def sign():
    return obj.user_signup_model()

@app.route("/users")
def getUsers():
    return obj.user_getails_model()

@app.route("/adduser" ,methods=["POST"])
def addUser():
    print(request.form)
    return obj.add_user_details_model(request.form)
    


@app.route('/deleteuser/<int:id>',methods=["DELETE"])
def delete_user(id):
    print(id)
    return obj.delete_user(id)