import mysql.connector
import json
from flask import make_response
class user_model:
    def __init__(self):
        #connecting to the mysql
       self.connection= mysql.connector.connect(host="localhost",user="root",password="root", database="mydb")
       self.cur= self.connection.cursor()
       if self.connection:
           print("connect created")
       else:
           print("not connected")
    def user_signup_model(self):
        return "This is signup from model"
   
    def user_getails_model(self):
        self.cur.execute("select * from user")
        result=self.cur.fetchall()
        if len(result)>0:
            #return json.dumps(result)
            #return {"payload":result}
            return make_response({"payload":result},200)
        else:
            #return "No Data found"
            return make_response({"message":"No data found"},204)
    
    def add_user_details_model(self,data):
        print(data['name'])
        insert_sql="insert into user(name,address) values(%s,%s)"
        data=(data['name'],data['address'])
        self.cur.execute(insert_sql,data)
        self.connection.commit()
        return {"message":"record inserted"}
    
    def delete_user(self,id):
        self.cur.execute(f"delete from user where id={id}")
        if self.cur.rowcount>0:
            return {"message":"User delete successfully"}
        else:
            return {"message":"User not delete successfully"}