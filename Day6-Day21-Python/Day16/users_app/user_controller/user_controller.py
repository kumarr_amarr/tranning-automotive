import app
import user_model.user_model as um
user_model = um.UserModel()

@app.user_app.route("/signup")
def signup():
    return user_model.user_signup()

@app.user_app.route("/users")
def get_all_users():
    users= user_model.get_users()
    return users
    
@app.user_app.route("/createUser", methods=["POST"])
def create_user():
    return user_model.create_user()

@app.user_app.route("/user")
def get_user():
    return user_model.get_user()

@app.user_app.route("/deleteUser", methods=["DELETE"])
def delete_user():
    return user_model.delete_user()

@app.user_app.route("/updateUser", methods=["PUT"])
def update_user():
    return user_model.update_user()
