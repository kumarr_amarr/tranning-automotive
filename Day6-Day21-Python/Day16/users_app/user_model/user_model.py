from mysql import connector
from flask import request, Response
import json
class UserModel:
    def __init__(self):
        self.connection=connector.connect(host="localhost", user="root", password="admin@123", database="user_db")
        self.cursor=self.connection.cursor()
        if self.connection:
            print("Database connected.")
        else:
            print("Check DB connection.")

    def user_signup(self):
        return "Signup completed."
    
    def get_users(self):
        self.cursor.execute("select * from user_model")
        users_data= self.cursor.fetchall()
        print(users_data)
        for row in users_data:
            users = []
            user = {"id": row[0], "name": row[1], "course": row[2] }
            users.append(user)
        return json.dumps(users)
    
    def create_user(self):
        data=request.get_json()
        id=data.get("id")
        name=data.get("name")
        course=data.get("course")
        self.cursor.execute("INSERT INTO user_model (id, name, course) VALUES (%s, %s, %s)", (id, name, course))
        self.connection.commit()
        return {"message":"record inserted"}
    
    def get_user(self):
        data=request.get_json()
        id=data.get("id")
        self.cursor.execute("SELECT * FROM user_model WHERE id = %s", (id,))
        user_data=self.cursor.fetchone()
        print(user_data)
        user = { "id": user_data[0], "name": user_data[1], "course": user_data[2] }
        return json.dumps(user)

    def delete_user(self):
        data=request.get_json()
        id=data.get("id")
        self.cursor.execute("DELETE FROM user_model WHERE id = %s", (id,))
        self.connection.commit()
        if self.cursor.rowcount > 0:
            return {"message": "Record deleted."}
        else:
            return {"message": "User not found."}
        
    def update_user(self):
        data=request.get_json()
        id=data.get("id")
        name = data.get('name')
        course = data.get('course')
        self.cursor.execute("SELECT * FROM user_model WHERE id = %s", (id,))
        user_data=self.cursor.fetchone()
        user = {"id": user_data[0]}
        if user.get("id")==id:
            self.cursor.execute("UPDATE user_model set name = %s, course = %s WHERE id = %s", (name, course, id))
            self.connection.commit()
            return {"message": "Record updated."}
        else:
            return {"message": "User not found to update."}