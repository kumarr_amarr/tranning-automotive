package com.demoappium;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.android.AndroidDriver;

public class ApkInstall{
	
	public static void main(String[] args) throws MalformedURLException{
		//TODO Auto-generated method stub 
		DesiredCapabilities capabilities = new DesiredCapabilities();
		
		capabilities.setCapability("platformName", "Android");
		capabilities.setCapability("appium:automationName", "uiautomator2");
		capabilities.setCapability("appium:deviceName", "vivo V2312");
		capabilities.setCapability("appium:app", "D:\\AppiumLab\\Apks\\TAF.apk");
		
		URL url = URI.create("http://127.0.0.1:4723/").toURL();
		AndroidDriver driver = new AndroidDriver (url, capabilities);
		driver.quit();
		System.out.println("App Successfully installed.");
	}
}
