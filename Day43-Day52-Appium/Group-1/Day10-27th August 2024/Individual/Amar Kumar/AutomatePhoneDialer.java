package com.demoappium;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.android.AndroidDriver;

public class AutomatePhoneDialer {

	public static void main(String[] args) throws MalformedURLException {
		// TODO Auto-generated method stub
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("platformName", "Android");
		capabilities.setCapability("appium:automationName", "uiautomator2");
		capabilities.setCapability("appium:deviceName", "vivo V2312");
		capabilities.setCapability("appium:appPackage", "com.google.android.dialer");
		capabilities.setCapability("appium:appActivity", "com.google.android.dialer.extensions.GoogleDialtactsActivity");
		
		URL url = URI.create("http://127.0.0.1:4723/").toURL();
		
		AndroidDriver driver = new AndroidDriver(url, capabilities);
		
		System.out.println("Phone application has started.");
		
		WebElement dialPad = driver.findElement(By.id("com.google.android.dialer:id/dialpad_fab")); dialPad.click();
		WebElement eight =driver.findElement(By.id("com.google.android.dialer:id/eight"));eight.click();
		WebElement two =driver.findElement(By.id("com.google.android.dialer:id/two")); two.click();
		WebElement nine =driver.findElement(By.id("com.google.android.dialer:id/nine")); nine.click();
		eight.click();
		WebElement seven =driver.findElement(By.id("com.google.android.dialer:id/seven")); seven.click();
		WebElement five =driver.findElement(By.id("com.google.android.dialer:id/five")); five.click();
		WebElement four =driver.findElement(By.id("com.google.android.dialer:id/four")); four.click();
		nine.click();
		four.click();
		seven.click();
		WebElement dialpad_voice_call_button =driver.findElement(By.id("com.google.android.dialer:id/dialpad_voice_call_button"));
		dialpad_voice_call_button.click();
		
		System.out.println("Calling.............");
		driver.quit();
	}
}
