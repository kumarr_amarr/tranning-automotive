class VolumeExceed{

    public void setVolume(int volume) {
        if (volume > MAX_VOLUME_LIMIT) {
            volume = MAX_VOLUME_LIMIT;
        }
        this.currentVolume = volume;
        updateMusicSystemVolume(volume);
    }


    public void setVolume(int volume) {
        if (volume > MAX_VOLUME_LIMIT) {
            volume = MAX_VOLUME_LIMIT;
        }
        this.currentVolume = volume;
        try {
            updateMusicSystemVolume(volume);
        } catch (SystemException e) {
            logError("Failed to update volume on the music system.", e);
            revertToPreviousVolume();
         }
    }
}
