package com.demoappium;


import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class BiometricAccess {

	// Data provider to supply biometric test data
	@DataProvider(name = "biometricData")
	public Object[][] biometricData() {
		return new Object[][]{
			{"validFingerprint", true},
			{"invalidFingerprint", false}
		};
	}

	// Test method using the provided biometric data
	@Test(dataProvider = "biometricData")
	public void testBiometricAccess(String fingerprint, boolean expectedResult) {
		// Test logic for biometric authentication
		boolean result = authenticateWithBiometric(fingerprint);
		Assert.assertEquals(result, expectedResult, "Biometric authentication failed.");
	}

	// Method to simulate biometric authentication
	private boolean authenticateWithBiometric(String fingerprint) {
		// Simulate authentication logic (Replace with actual implementation)
		// For demo purposes, assume "validFingerprint" is always valid
		// and anything else is invalid.

		if ("validFingerprint".equals(fingerprint)) {
			return true; // Simulate successful authentication
		} else {
			return false; // Simulate failed authentication
		}
	}
}

