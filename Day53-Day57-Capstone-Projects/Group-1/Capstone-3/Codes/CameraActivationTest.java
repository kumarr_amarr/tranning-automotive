package com.demoappium;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;


import io.appium.java_client.android.AndroidDriver;

public class CameraActivationTest {

	@Test
	public void testCameraActivation() throws MalformedURLException {
		DesiredCapabilities capabilities = new DesiredCapabilities();

		capabilities.setCapability("platformName", "Android");
		capabilities.setCapability("automationName", "uiautomator2");
		capabilities.setCapability("deviceName", "vivo V2312");
		capabilities.setCapability("appPackage", "com.vivo.camera");
		capabilities.setCapability("appActivity", "com.vivo.camera.Camera");

		URL url = URI.create("http://127.0.0.1:4723/wd/hub").toURL();
		AndroidDriver driver = new AndroidDriver(url, capabilities);

		// Navigate to the camera screen
		WebElement cameraButton = driver.findElement(By.id("com.example.automotive.security:id/cameraButton"));
		cameraButton.click();

		// Validate camera activation
		WebElement cameraView = driver.findElement(By.id("com.example.automotive.security:id/cameraView"));
		Assert.assertTrue(cameraView.isDisplayed(), "Camera did not activate as expected.");

		// Quit the driver
		driver.quit();
	}
}
